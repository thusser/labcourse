"""
Django settings for LabCourse project.

Generated by 'django-admin startproject' using Django 4.0.3.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.0/ref/settings/
"""

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "labcourse",
        "USER": "postgres",
        "PASSWORD": "postgres",
        "HOST": "postgres",
        "PORT": 5432,
    }
}

STATIC_ROOT = "/static/"

AUTHLIB_OAUTH_CLIENTS = {
    "gwdg": {
        "server_metadata_url": "http://localhost:8080/realms/labcourse/.well-known/openid-configuration",
        "client_id": "labcourse",
        "client_secret": "wQws45m47hBwTp5UbKh3b7jwa47KriHv",
    }
}
