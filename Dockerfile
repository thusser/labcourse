FROM python:3.9-slim
WORKDIR /labcourse
COPY . /labcourse/
RUN bash -c "pip install poetry && \
             poetry config virtualenvs.create false && \
             poetry install"
CMD  bash -c "python manage.py collectstatic --no-input && \
              cp -r media/* /media/ && \
              python manage.py migrate && \
              gunicorn --bind 0.0.0.0:8000 --workers=6 --threads=3 --worker-class=gthread LabCourse.wsgi"
