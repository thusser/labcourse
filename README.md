# LabCourse

This is the code for the homepage of the Advanced Lab Course in Physics at Goettingen University.


## Development

There are two different options for running a development version of the LabCourse webpage: directly and in a 
Docker container.

### Private media files

Some private media files are not part of this repository. Ask its maintainer to get access.


### Dev server

Copy the file `settings.py.sample` to `settings.py` and add/change options if required. You can add your current 
IP to `ALLOWED_HOSTS`. The database is changed to a local sqlite database. All the static/media settings make sure 
that static data is served without a reverse proxy. Also change the last line to the IP/hostname of the machine 
running it, in case you want to access the website from another computer.

You can easily start the server via:

    ./manage.py runserver

If you want to run KeyCloak as well, it's advisable to use the available docker-compose file in the docker directory.
Add this to your `local_settings.py`:

    AUTHLIB_OAUTH_CLIENTS = {
        "gwdg": {
            "server_metadata_url": "http://localhost:8080/realms/<realm>/.well-known/openid-configuration",
            "client_id": "<id>",
            "client_secret": "<secret>",
        }
    }

Fill `<realm>`, `<id>`, `<secret>` with the correct values that you set up in KeyCloak.
Then go to the docker directory and just run (after installing Docker and docker-compose):

    docker-compose up postgres keycloak


### Docker
The easiest way to get everything running is to use Docker. First build the image (in the root directory):

    docker build . -t labcourse

Then go into the docker directory and run:

    docker-compose up


