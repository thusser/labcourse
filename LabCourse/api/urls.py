"""LabCourse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from .views import semester_dates, semester_students, experiment_instructions, search_user

urlpatterns = [
    path("semester/<str:semester>/dates/", semester_dates),
    path("semester/<str:semester>/students/", semester_students),
    path("experiment/<str:experiment>/instructions/", experiment_instructions),
    path("search/user/", search_user),
]
