# Generated by Django 4.0.4 on 2022-12-08 14:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0049_labuser_student_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='labuser',
            name='calendar_token',
            field=models.CharField(default=None, max_length=20, null=True),
        ),
    ]
