# Generated by Django 4.0.4 on 2022-08-02 09:19

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0029_alter_exam_examiner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='experiment',
            name='examiners',
            field=models.ManyToManyField(blank=True, limit_choices_to={'is_examiner': True}, related_name='examiner_experiments', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='experiment',
            name='tutors',
            field=models.ManyToManyField(blank=True, limit_choices_to={'is_tutor': True}, related_name='tutor_experiments', to=settings.AUTH_USER_MODEL),
        ),
    ]
