from django.contrib import admin
from django.contrib.admin import display
from django.contrib.admin.templatetags.admin_urls import admin_urlname
from django.contrib.auth import password_validation
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm, ReadOnlyPasswordHashField, UserChangeForm
from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm, SelectMultiple
from django.shortcuts import resolve_url
from django.template.defaultfilters import truncatechars
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _

from .models import (
    Experiment,
    Semester,
    SemesterDate,
    Category,
    LabUser,
    Appointment,
    Report,
    AppointmentRequest,
    JoinRequest,
    Exam,
    ExperimentInstructions,
    ReportVersion,
    AppointmentUpdate,
    ReportUpdate,
    ExamUpdate,
    UpdateModel,
    Content,
    SafetyBriefing,
)


@admin.register(JoinRequest)
class JoinRequestAdmin(admin.ModelAdmin):
    pass


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Experiment)
class ExperimentAdmin(admin.ModelAdmin):
    list_display = (
        "code",
        "name",
        "categories_list",
        "location",
        "language",
        "num_tutors",
        "num_examiners",
        "comment",
        "active",
    )
    search_fields = ("code", "name", "categories__code")
    filter_horizontal = ["tutors", "examiners"]


@admin.register(ExperimentInstructions)
class ExperimentInstructionsAdmin(admin.ModelAdmin):
    list_display = ("experiment", "version", "filename", "filesize", "date", "user")
    search_fields = ("experiment__code", "experiment__name", "filename", "user__last_name", "user__first_name")


class SemesterForm(ModelForm):
    class Meta:
        model = Semester
        fields = "__all__"
        widgets = {
            "semester": forms.Select(choices=((0, "Winter term"), (1, "Summer term"))),
            "week_day": forms.Select(
                choices=(
                    (0, "Monday"),
                    (1, "Tuesday"),
                    (2, "Wednesday"),
                    (3, "Thursday"),
                    (4, "Friday"),
                    (5, "Saturday"),
                    (6, "Sunday"),
                )
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["code"].required = False


@admin.register(Semester)
class SemesterAdmin(admin.ModelAdmin):
    form = SemesterForm


@admin.register(SemesterDate)
class SemesterDateAdmin(admin.ModelAdmin):
    list_display = ("get_semester", "date", "special")

    @display(ordering="semester__code", description="Semester")
    def get_semester(self, obj):
        return obj.semester.code


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ("get_date", "get_experiment", "get_students", "is_open")
    search_fields = ("date__date", "experiment__code", "students__last_name")
    filter_horizontal = ["students"]

    @display(ordering="date__date", description="Date")
    def get_date(self, obj):
        return obj.date.date

    @display(ordering="experiment__code", description="Experiment")
    def get_experiment(self, obj):
        return obj.experiment.code

    @display(ordering="students__last_name", description="Student")
    def get_students(self, obj):
        return ", ".join([s.short_name for s in obj.students.all()])


@admin.register(AppointmentRequest)
class AppointmentRequestAdmin(admin.ModelAdmin):
    list_display = ("get_student", "get_experiment", "get_date", "get_partners", "comment")
    search_fields = ("student__last_name", "experiment__code", "date__date", "partners__last_name", "comment")
    filter_horizontal = ["partners"]

    @display(ordering="student__last_name", description="Student")
    def get_student(self, obj):
        return obj.student.full_name

    @display(ordering="experiment__code", description="Experiment")
    def get_experiment(self, obj):
        return obj.experiment.code

    @display(ordering="date__date", description="Date")
    def get_date(self, obj):
        return obj.date.date

    @display(ordering="partners|count", description="Partners")
    def get_partners(self, obj):
        return ", ".join([s.short_name for s in obj.partners.all()])


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ("get_semester", "get_student", "get_tutor", "get_experiment", "get_date")
    search_fields = (
        "appointment__date__semester__code",
        "student__last_name",
        "tutor__last_name",
        "appointment__experiment__code",
        "appointment__date__date",
    )

    @display(ordering="appointment__date__semester__code", description="Semester")
    def get_semester(self, obj):
        return obj.appointment.date.semester.code

    @display(ordering="student__last_name", description="Student")
    def get_student(self, obj):
        return obj.student.full_name

    @display(ordering="tutor__last_name", description="Tutor")
    def get_tutor(self, obj):
        return None if obj.tutor is None else obj.tutor.full_name

    @display(ordering="appointment__experiment__code", description="Experiment")
    def get_experiment(self, obj):
        return obj.appointment.experiment.code

    @display(ordering="appointment__date__date", description="Date")
    def get_date(self, obj):
        return obj.appointment.date.date


@admin.register(ReportVersion)
class ReportVersionAdmin(admin.ModelAdmin):
    list_display = ("report", "version", "date")
    search_fields = (
        "report__student__last_name",
        "report__student__first_name",
        "report__appointment__experiment__code",
        "report__appointment__date__semester__code",
    )


class LabUserCreationForm(UserCreationForm):
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        required=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        strip=False,
        required=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    def save(self, commit=True):
        user = forms.ModelForm.save(self, commit=False)
        if self.cleaned_data["password1"]:
            user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class LabUserChangeForm(UserChangeForm):
    # is_tutor = forms.BooleanField(required=False, label="Tutor")
    # is_examiner = forms.BooleanField(required=False, label="Examiner")

    password = ReadOnlyPasswordHashField(
        label=_("Password"),
        help_text=_(
            "Raw passwords are not stored, so there is no way to see this "
            "user’s password, but you can change the password using "
            '<a href="{}">this form</a>.'
        ),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(LabUserChangeForm, self).__init__(*args, **kwargs)
        user = LabUser.objects.get(pk=self.instance.pk)
        self.fields["is_tutor"].initial = user.is_tutor
        self.fields["is_examiner"].initial = user.is_examiner
        self.fields["student_number"].required = False

        # show labs
        tutor_labs = user.tutor_experiment_list()
        self.fields["is_tutor"].help_text = f"Labs: {tutor_labs}" if tutor_labs else ""
        examiner_labs = user.examiner_experiment_list()
        self.fields["is_examiner"].help_text = f"Labs: {examiner_labs}" if examiner_labs else ""
        delegate_for = [f'<a href="/admin/api/labuser/{d.pk}/">{d.full_name}</a>' for d in user.delegate_for.all()]
        self.fields["delegates"].help_text = "Delegate for: " + ", ".join(delegate_for)

    def clean(self):
        # get values for tutor/examiner
        cleaned_data = super().clean()
        is_tutor = cleaned_data.get("is_tutor")
        is_examiner = cleaned_data.get("is_examiner")

        # get user
        user = self.instance

        # no de-selection allowed, if labs are assigned
        if user.is_tutor and user.tutor_experiments.count() > 0 and not is_tutor:
            raise ValidationError("You cannot deselect Tutor while user is assigned to a lab!")
        if user.is_examiner and user.examiner_experiments.count() > 0 and not is_examiner:
            raise ValidationError("You cannot deselect Examiner while user is assigned to a lab!")

        # return super
        return super().clean()

    class Meta:
        model = LabUser
        fields = "__all__"


@admin.register(LabUser)
class LabUserAdmin(UserAdmin):
    add_form = LabUserCreationForm
    form = LabUserChangeForm
    list_display = (
        "username",
        "title",
        "first_name",
        "last_name",
        "institute",
        "room",
        "phone",
        "is_tutor",
        "is_examiner",
        "is_admin",
    )
    list_filter = ("is_staff", "is_superuser", "is_active", "groups", "is_tutor", "is_examiner", "is_admin")

    fieldsets = (
        (None, {"fields": ("username", "password")}),
        ("Personal info", {"fields": ("title", "first_name", "last_name", "email")}),
        ("Contact", {"fields": ("institute", "address", "room", "phone", "student_number", "comment")}),
        ("Lab Course", {"fields": ("is_tutor", "is_examiner", "is_admin", "student_semesters", "delegates")}),
        (
            "Permissions",
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        ("Important dates", {"fields": ("last_login", "date_joined")}),
    )

    add_fieldsets = (
        (None, {"fields": ("username", "password1", "password2")}),
        ("Personal info", {"fields": ("title", "first_name", "last_name", "email")}),
        ("Contact", {"fields": ("institute", "address", "room", "phone", "student_number", "comment")}),
        ("Lab Course", {"fields": ("is_tutor", "is_examiner")}),
        (
            "Permissions",
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        ("Important dates", {"fields": ("last_login", "date_joined")}),
    )

    filter_horizontal = ["delegates"]

    def save_model(self, request, obj, form, change):
        # save model
        super().save_model(request, obj, form, change)

        # get values for tutor and examiner
        is_tutor = form.cleaned_data.get("is_tutor", False)
        is_examiner = form.cleaned_data.get("is_examiner", False)

        # changes?
        if is_tutor != obj.is_tutor:
            obj.set_tutor(is_tutor)
        if is_examiner != obj.is_examiner:
            obj.set_examiner(is_examiner)


@admin.register(Exam)
class ExamAdmin(admin.ModelAdmin):
    list_display = ("student", "examiner", "date", "get_experiments", "active")
    filter_horizontal = ["reports"]

    @display(description="Experiments")
    def get_experiments(self, obj):
        return ", ".join([r.appointment.experiment.code for r in obj.reports.all()])


@admin.register(UpdateModel)
class UpdateAdmin(admin.ModelAdmin):
    list_display = ("get_date", "get_subclass_link", "get_action", "what", "user", "comment")

    @display(description="date")
    def get_date(self, obj):
        return obj.date.strftime("%Y-%m-%d %H:%I:%S")

    @display(description="type")
    def get_subclass_link(self, obj):
        # get subclass
        sc = obj.obj()

        # decide on class
        if isinstance(sc, AppointmentUpdate):
            url = resolve_url(admin_urlname(Appointment._meta, "change"), sc.appointment.id)
        elif isinstance(sc, ExamUpdate):
            url = resolve_url(admin_urlname(Exam._meta, "change"), sc.exam.id)
        elif isinstance(sc, ReportUpdate):
            url = resolve_url(admin_urlname(Report._meta, "change"), sc.report.id)
        else:
            raise ValueError

        klass = Appointment if sc == "Appointment" else "Exam" if sc == "Exam" else "Report"

        # get URL
        return format_html(
            '<a href="{url}">{name}</a>'.format(url=url, name=sc.__class__.__name__.replace("Update", ""))
        )

    @display(description="action")
    def get_action(self, obj):
        return obj.action


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display = ("key", "short_value")

    @display(description="Content")
    def short_value(self, obj):
        return truncatechars(obj.value, 100)


@admin.register(SafetyBriefing)
class SafetyBriefingAdmin(admin.ModelAdmin):
    list_display = ("date",)
    filter_horizontal = ["students"]
