import json
import logging

from django.contrib.auth.decorators import user_passes_test
from django.contrib.postgres.search import SearchVector, SearchQuery
from django.db import connection
from django.db.models import Q
from django.http import JsonResponse, Http404

from LabCourse.api.models import Semester, Experiment, LabUser


@user_passes_test(lambda u: u.is_authenticated)
def semester_dates(request, semester):
    semester = Semester.objects.get(code=semester.replace("-", "/"))
    dates = [{"pk": d.pk, "date": d.date.strftime("%A, %-d %B %Y")} for d in semester.semesterdate_set.all()]
    return JsonResponse({"dates": dates})


@user_passes_test(lambda u: u.is_authenticated)
def semester_students(request, semester):
    semester = Semester.objects.get(code=semester.replace("-", "/"))
    students = [{"pk": s.pk, "name": s.full_name} for s in semester.students.all()]
    return JsonResponse({"students": students})


@user_passes_test(lambda u: u.is_authenticated)
def experiment_instructions(request, experiment):
    experiment = Experiment.objects.get(code=experiment.upper())
    instructions = [
        {
            "pk": i.pk,
            "version": i.version,
            "filename": i.filename.split("/")[1],
            "url": i.url,
            "filesize": i.filesize_kib,
            "date": i.date.strftime("%Y-%m-%d"),
            "author": i.user.short_name,
        }
        for i in experiment.experimentinstructions_set.all()
    ]
    return JsonResponse({"instructions": instructions})


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def search_user(request):
    # get search entry
    body = json.loads(request.body)
    if "search" not in body:
        raise Http404()
    search = body["search"].strip()

    # in PostgreSQL we can do a full-text search, while for sqlite this needs to be simpler
    if len(search) < 3:
        result = []
    elif connection.vendor == "postgresql":
        result = LabUser.objects.annotate(
            search=SearchVector("first_name", "last_name"),
        ).filter(search=SearchQuery(search))
    elif connection.vendor == "sqlite":
        result = LabUser.objects.filter(Q(first_name__icontains=search) | Q(last_name__icontains=search))
    else:
        logging.error(f"Unknown db vendor: {connection.vendor}.")
        raise Http404()

    # build users
    users = [
        {
            "id": u.pk,
            "name": u.full_name,
            "student": ", ".join([s.code for s in u.student_semesters.all()]),
            "tutor": u.tutor_experiment_list(),
            "examiner": u.examiner_experiment_list(),
        }
        for u in result
    ]

    # return
    return JsonResponse({"users": users})
