import datetime
import logging

from django.core.management.base import BaseCommand
from django.db import transaction

from LabCourse.api.models import Appointment


class Command(BaseCommand):
    help = "Close appointments that are in the past"

    @transaction.atomic
    def handle(self, *args, **options):
        # loop all open appointments that are for today or earlier
        logging.info("Fetching open appointments...")
        appointments = Appointment.objects.filter(is_open=True, date__date__lte=datetime.date.today())
        for app in appointments:
            logging.info(
                f"Closing appointment for {app.experiment.code} on {app.date.date.strftime('%Y-%m-%d')} for "
                f"{len(app.students.all())} student(s)..."
            )
            app.close_appointment()
        logging.info(f"Closed {len(appointments)} appointment(s).")
