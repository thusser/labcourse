from pathlib import Path

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from LabCourse.api.models import Experiment, Category, LabUser, Semester
import pandas as pd


class Command(BaseCommand):
    help = "Import students"

    def add_arguments(self, parser):
        parser.add_argument("path", type=str)

    @transaction.atomic
    def handle(self, *args, **options):
        # load data
        path = Path(options["path"])
        data = pd.read_csv(path, sep=";", index_col=False)

        # get semester
        semester = Semester.objects.get(year=2022, semester=1)

        # loop students
        for _, row in data.iterrows():
            # try to find student
            try:
                LabUser.objects.get(username=row["Nutzernamen"])
                print(f"User {row['Nutzernamen']} already exists.")
                continue
            except LabUser.DoesNotExist:
                pass

            # create it
            student = LabUser()
            student.username = row["Nutzernamen"]
            student.first_name = row["Vorname"]
            student.last_name = row["Nachname"]
            student.email = row["E-Mail"]
            student.save()
            student.student_semesters.add(semester)
            print(f"Added user {row['Nutzernamen']}.")
