from pathlib import Path

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from LabCourse.api.models import Experiment, Category, Examiner, Tutor, LabUser
import pandas as pd


class Command(BaseCommand):
    help = "Init and import from old homepage"

    def add_arguments(self, parser):
        parser.add_argument("path", type=str)

    def handle(self, *args, **options):
        path = Path(options["path"])

        self._add_categories()
        self._import_experiments(path)
        self._import_tutors(path)

    def _add_categories(self):
        Category.objects.get_or_create(
            code="KT",
            defaults={
                "name": "nuclear physics and particle physics",
                "color": "#FFE1E1",
            },
        )
        Category.objects.get_or_create(
            code="FM",
            defaults={
                "name": "solid state physics and physics of materials",
                "color": "#FFFFC8",
            },
        )
        Category.objects.get_or_create(
            code="BK",
            defaults={"name": "biophysics and complex systems", "color": "#E1FFE1"},
        )
        Category.objects.get_or_create(
            code="AG",
            defaults={"name": "astrophysics and geophysics", "color": "#E1E1FF"},
        )

    def _import_experiments(self, path):
        # read CSV
        d = pd.read_csv(path / "daten" / "Versuchsliste.csv", skiprows=1)

        # iter rows
        for _, row in d.iterrows():
            # exists?
            try:
                Experiment.objects.get(code=row["VERS_ID"])
                continue
            except Experiment.DoesNotExist:
                pass

            # create experiment
            exp = Experiment()

            # get data from table
            exp.code = row["VERS_ID"]
            exp.name = row["VERS_TITEL_LANG"]
            exp.language = row["VERS_SPRACHEN"]
            exp.room = row["VERS_RAUM"]
            if len(str(row["VERS_BEMERK"])) > 0:
                exp.comment = row["VERS_BEMERK"]

            # get description
            inst = exp.code.split(".")[0].lower()
            with open(path / "versuche" / inst / row["VERS_ABSTR_FN"]) as f:
                exp.desc = f.read()

            # save, add cats, save again
            exp.save()
            exp.categories.set(
                [
                    Category.objects.get(code=cat.upper())
                    for cat in row["VERS_SPE"].split(",")
                ]
            )
            exp.save()

    def _import_tutors(self, path):
        self._import_tutors_examiners(path, "BETR", True)
        self._import_tutors_examiners(path, "PRUE", False)

    def _import_tutors_examiners(self, path, prefix: str, tutor: bool):
        # read CSV
        if tutor:
            d = pd.read_csv(path / "daten" / "Betreuer.csv", skiprows=1)
        else:
            d = pd.read_csv(path / "daten" / "Pruefer.csv", skiprows=1)

        # iter rows
        for _, row in d.iterrows():
            # get user name
            s = row[f"{prefix}_NAME_LANG"].split(" ")
            titles = []
            while s[0] in ["Prof.", "Dr.", "apl.", "PD"]:
                titles.append(s[0])
                s = s[1:]
            title = " ".join(titles) if titles else None
            first_name = " ".join(s[:-1])
            last_name = s[-1]

            # get/create user
            user, _ = LabUser.objects.get_or_create(
                email=row[f"{prefix}_EMAIL"],
                defaults={
                    "username": row[f"{prefix}_EMAIL"],
                    "first_name": first_name,
                    "last_name": last_name,
                    "title": title,
                },
            )

            # fill defaults
            defaults = {
                "institute": row[f"{prefix}_INST"],
                "address": row[f"{prefix}_ADR"],
                "room": row["BETR_RAUMNR"] if tutor else row["PRUE_RAUM"],
                "phone": row[f"{prefix}_TEL"],
            }

            # get tutor or examiner, if exists, otherwise create
            model = Tutor if tutor else Examiner
            te, _ = model.objects.get_or_create(user=user, defaults=defaults)

            # get experiment and add it
            try:
                code = row[f"{prefix}_VERSID"]
                lab = Experiment.objects.get(code=code)
            except Experiment.DoesNotExist:
                raise ValueError(f"{code} does not exist.")
            if tutor:
                lab.tutors.add(te)
            else:
                lab.examiners.add(te)
