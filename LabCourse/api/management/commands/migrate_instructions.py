import os
from pathlib import Path

from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from LabCourse import settings
from LabCourse.api.models import Experiment, Category, LabUser, Semester
import pandas as pd


class Command(BaseCommand):
    help = "Migrate instructions"

    def add_arguments(self, parser):
        parser.add_argument("path", type=str)
        parser.add_argument("user", type=str)

    @transaction.atomic
    def handle(self, *args, **options):
        # get old path to instructions
        path = options["path"]

        # get user
        user = LabUser.objects.get(username=options["user"])

        # loop all experiments
        for experiment in Experiment.objects.all():
            # build old filename
            filename = f"{experiment.category.lower()}/{experiment.code.lower()}.pdf"
            filepath = os.path.join(path, filename)
            print(filepath)
            if not os.path.exists(filepath):
                print("  File missing.")
                continue

            # read it
            with open(filepath, "rb") as f:
                content = f.read()

            # create django uploaded file
            uploaded = SimpleUploadedFile(filename, content, content_type="application/pdf")

            # add it to experiment
            experiment.add_instructions(uploaded, user)
            print("  Done.")
