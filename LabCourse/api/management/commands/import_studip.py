from django.core.management.base import BaseCommand
from django.db import transaction

from LabCourse.api.models import LabUser
import pandas as pd


def import_from_studip(fn):
    # read all
    df = pd.read_csv(fn, sep=";")

    # find rows with table names and add length as last index
    table_names = ["Lehrende", "Tutor/-innen", "Studierende"]
    index_positions = df[df["Anrede"].isin(table_names)].index.tolist() + [len(df)]

    # extract tables
    tables = {
        table_names[i]: df[index_positions[i] + 1 : index_positions[i + 1]] for i in range(len(index_positions) - 1)
    }

    # get students
    students = tables["Studierende"].copy()

    # loop students
    rows_to_drop = []
    for idx, row in students.iterrows():
        # try to find student
        try:
            user = LabUser.objects.get(username=row["Nutzernamen"])
        except LabUser.DoesNotExist:
            rows_to_drop.append(idx)
            continue

        # set student number
        try:
            user.student_number = int(row["matrikelnummer"])
            user.save()
        except ValueError:
            rows_to_drop.append(idx)
            continue

    # drop rows from table and return
    students.drop(rows_to_drop, inplace=True)
    print(students)
    return students


class Command(BaseCommand):
    help = "Import students from studip"

    def add_arguments(self, parser):
        parser.add_argument("path", type=str)

    @transaction.atomic
    def handle(self, *args, **options):
        # load data
        with open(options["path"]) as fn:
            import_from_studip(fn)
