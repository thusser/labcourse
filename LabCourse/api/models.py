from __future__ import annotations
import datetime
import os
import random
import re
import string
from enum import Enum
from typing import Optional, List

from django.contrib import admin
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.contrib.auth.models import AbstractUser, User
from django.db.models import Q, Max
from django.db.transaction import atomic

from LabCourse import settings


class Semester(models.Model):
    class Meta:
        unique_together = (("year", "semester"),)
        ordering = (
            "year",
            "-semester",
        )

    year = models.IntegerField()
    semester = models.IntegerField()  # 0=WiSe, 1=SoSe
    code = models.CharField(max_length=10)
    lecture_start = models.DateField()
    lecture_end = models.DateField()
    exam_date = models.DateField()
    week_day = models.IntegerField()  # 0=Mon, 1=Tue, ...
    active = models.BooleanField(default=True)

    def __str__(self):
        if self.semester == 0:
            return f"Winter Term {self.year}/{str(self.year + 1)[-2:]}"
        else:
            return f"Summer Term {self.year}"

    def short_name(self):
        if self.semester == 0:
            return f"WiSe{str(self.year)[-2:]}/{str(self.year + 1)[-2:]}"
        else:
            return f"SoSe{self.year}"

    @property
    def safe_code(self) -> str:
        return self.code.replace("/", "-")

    @property
    def start(self):
        return datetime.date(year=self.year, month=10 if self.semester == 0 else 4, day=1)

    @property
    def end(self):
        return (
            datetime.date(year=self.year + 1, month=3, day=31)
            if self.semester == 0
            else datetime.date(year=self.year, month=9, day=30)
        )

    def save(self, *args, **kwargs):
        # set code
        self.code = self.short_name()

        # did the dates change?
        need_create_dates = True
        if self.pk is not None:
            orig = Semester.objects.get(pk=self.pk)
            need_create_dates = (
                self.lecture_start != orig.lecture_start
                or self.lecture_end != orig.lecture_end
                or self.week_day != orig.week_day
            )

        # save model
        super().save(*args, **kwargs)

        # deactivate all semesters except this one
        Semester.objects.exclude(id=self.id).update(active=False)

        # create dates?
        if need_create_dates:
            self._create_dates()

    @staticmethod
    def current():
        """Return current semester."""

        # get date
        now = datetime.datetime.now()

        # check month
        if 4 <= now.month <= 9:
            # summer semester
            semester = 1
            year = now.year

        else:
            # winter semester
            semester = 0
            year = now.year if now.month >= 10 else now.year - 1

        # get and return semester
        return Semester.objects.get(semester=semester, year=year)

    def _create_dates(self):
        # delete all dates
        SemesterDate.objects.filter(semester=self).delete()

        # loop all dates
        date = self.lecture_start
        while date <= self.lecture_end:
            # correct day of week?
            if date.weekday() == self.week_day:
                # create it
                SemesterDate(semester=self, date=date).save()

            # next day
            date += datetime.timedelta(days=1)


class SemesterDate(models.Model):
    class Meta:
        unique_together = (("semester", "date"),)
        ordering = ("date",)

    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
    date = models.DateField()
    special = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.date} ({self.semester})"


class LabUser(AbstractUser):
    class Meta:
        ordering = (
            "last_name",
            "first_name",
        )

    title = models.CharField(max_length=15, null=True, blank=True, default=None)
    institute = models.CharField(max_length=50, null=True, blank=True, default=None)
    address = models.CharField(max_length=50, null=True, blank=True, default=None)
    room = models.CharField(max_length=30, null=True, blank=True, default=None)
    phone = models.CharField(max_length=30, null=True, blank=True, default=None)
    is_tutor = models.BooleanField(default=False)
    is_examiner = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    student_semesters = models.ManyToManyField(Semester, related_name="students", blank=True)
    student_number = models.IntegerField(null=True)
    calendar_token = models.CharField(max_length=20, null=True, default=None)
    comment = models.TextField(null=True, blank=True, default=None)
    delegates = models.ManyToManyField("self", related_name="delegate_for", symmetrical=False, blank=True)

    def __str__(self):
        return self.full_name

    @property
    def is_student(self) -> bool:
        """Is true, if assigned to at least one semester."""
        return self.student_semesters.count() > 0

    @property
    def is_student_now(self) -> bool:
        """Is true, if assigned to current semester."""
        semester = Semester.current()
        return semester in self.student_semesters.all()

    def was_tutor(self) -> bool:
        """Is true, if user has been a tutor before or is now."""

        # either the user is a tutor now, or he/she has >0 reports
        return self.is_tutor or self.tutor_reports.count() > 0

    def was_examiner(self) -> bool:
        """Is true, if user has been a examiner before or is now."""

        # either the user is an examiner now, or he/she has >0 exams
        return self.is_examiner or self.examiner_exams.count() > 0

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        if self.title:
            full_name = f"{self.title} {full_name}"
        return full_name.strip()

    @property
    def full_name(self):
        return self.get_full_name()

    @property
    def short_name(self):
        first_name = "".join([f"{n[0]}." for n in re.split(r"[\s-]", self.first_name)])
        full_name = "%s %s" % (first_name, self.last_name)
        if self.title:
            full_name = f"{self.title} {full_name}"
        return full_name.strip()

    @property
    def code(self):
        return self.short_name.replace(" ", "").replace(".", "").lower()

    def tutored_experiments(self):
        """Return all experiments the user currently tutors or tutored in the past."""

        # all current experiments
        current = self.tutor_experiments.all()

        # past experiments
        past = Experiment.objects.filter(appointment__report__in=self.tutor_reports.all())

        # combine
        return (current | past).distinct()

    @property
    def tutor_experiments_all(self):
        return self.tutor_experiments.all()

    @admin.display(description="Tutor labs")
    def tutor_experiment_list(self):
        return ", ".join([e.code for e in self.tutor_experiments.all()]) if self.is_tutor else ""

    @property
    def examiner_experiments_all(self):
        return self.examiner_experiments.filter(active=True).all()

    @admin.display(description="Examiner labs")
    def examiner_experiment_list(self):
        return ", ".join([e.code for e in self.examiner_experiments.all()]) if self.is_examiner else ""

    def requested_join(self) -> bool:
        return self.joinrequest_set.count() > 0

    def create_calendar_token(self) -> None:
        # create new token
        letters = string.digits + string.ascii_letters
        token = "".join(random.choice(letters) for i in range(20))

        # set it
        self.calendar_token = token
        self.save()

    def history(self):
        # appointment updates, either as student or as tutor
        updates = list(
            AppointmentUpdate.objects.filter(
                Q(appointment__students__in=[self]) | Q(appointment__experiment__tutors__in=[self])
            ).distinct()
        )

        # report updates, either as student or as approving tutor
        updates += list(
            ReportUpdate.objects.filter(
                Q(report__appointment__students__in=[self]) | Q(report__appointment__experiment__tutors__in=[self])
            ).distinct()
        )

        # exam updates, either as student or as examiner
        updates += list(ExamUpdate.objects.filter(Q(exam__student=self) | Q(exam__examiner=self)).distinct())

        # sort by date
        return sorted(updates, key=lambda u: u.date)

    @property
    def last_safety_briefing(self) -> SafetyBriefing:
        return self.safety_briefings.order_by("date").last()

    @property
    def safety_briefing_valid(self) -> bool:
        sb = self.last_safety_briefing
        if sb is None:
            return False
        return datetime.date.today() < sb.date + datetime.timedelta(days=400)

    def is_user_or_delegate(self, user: LabUser) -> bool:
        if self.pk == user.pk:
            return True
        for delegate in user.delegates.all():
            if delegate.pk == self.pk:
                return True
        return False


class JoinRequest(models.Model):
    user = models.ForeignKey(LabUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.full_name


class Category(models.Model):
    class Meta:
        ordering = ("code",)

    code = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=50)
    color = models.CharField(max_length=7)

    def __str__(self):
        return f"{self.code} - {self.name}"

    @property
    def active_experiments(self):
        return Experiment.objects.filter(categories__pk=self.pk, active=True)


class Experiment(models.Model):
    class Meta:
        ordering = ("code",)

    code = models.CharField(max_length=10, primary_key=True)
    categories = models.ManyToManyField(Category)
    name = models.CharField(max_length=100)
    desc = models.TextField()
    language = models.CharField(max_length=10, default="E")
    location = models.CharField(max_length=100, blank=True, null=True, default=None)
    comment = models.CharField(max_length=100, blank=True, null=True, default=None)
    tutors = models.ManyToManyField(
        LabUser, related_name="tutor_experiments", blank=True, limit_choices_to={"is_tutor": True}
    )
    examiners = models.ManyToManyField(
        LabUser, related_name="examiner_experiments", blank=True, limit_choices_to={"is_examiner": True}
    )
    active = models.BooleanField()

    def __str__(self):
        return f"{self.code} - {self.name}"

    @property
    def image_url(self):
        return f"/media/experiments/{self.category.lower()}/{self.code.lower()}.png"

    @property
    def instructions_url(self):
        if self.experimentinstructions_set.count() == 0:
            return None
        filename = f"{self.category.lower()}/{self.code.lower()}.pdf"
        return os.path.join("/", settings.INSTRUCTIONS_URL, filename)

    @property
    def latest_instructions(self):
        return self.experimentinstructions_set.last()

    @property
    def main_category(self):
        return self.categories.first()

    @property
    def category(self):
        return self.code.split(".")[0]

    @property
    def sub_code(self):
        return self.code.split(".")[1]

    @property
    def institute(self):
        return self.location.split(",")[0] if self.location and "," in self.location else "N/A"

    @property
    def room(self):
        return self.location.split(",")[1] if self.location and "," in self.location else "N/A"

    @admin.display(description="Categories")
    def categories_list(self):
        return ", ".join([cat.code for cat in self.categories.all()])

    @admin.display(description="# Examiners")
    def num_examiners(self):
        return self.examiners.count()

    @admin.display(description="# Tutors")
    def num_tutors(self):
        return self.tutors.count()

    def is_tutor(self, user: User):
        return user in self.tutors.all()

    def is_examiner(self, user: User):
        return user in self.examiners.all()

    def add_instructions(self, f, user: LabUser):
        # get current highest version
        v = self.experimentinstructions_set.aggregate(Max("version"))
        version = v["version__max"] + 1 if v["version__max"] else 1

        # add new version
        instructions = ExperimentInstructions()
        instructions.experiment = self
        instructions.version = version
        instructions.date = datetime.datetime.now()
        instructions.user = user

        # create filename
        filename = f"{self.code.lower()}-v{version}.pdf"
        instructions.filename = os.path.join(self.category, filename)

        # create dir
        full_path = os.path.join(settings.INSTRUCTIONS_ROOT, instructions.filename)
        path = os.path.dirname(full_path)
        if not os.path.exists(path):
            os.makedirs(path)

        # save it
        size = 0
        with open(full_path, "wb+") as destination:
            for chunk in f.chunks():
                size += destination.write(chunk)

        # set size and save
        instructions.filesize = size
        instructions.save()


class ExperimentInstructions(models.Model):
    class Meta:
        ordering = ("version",)
        unique_together = (("experiment", "version"),)

    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    version = models.IntegerField()
    filename = models.CharField(max_length=100)
    filesize = models.IntegerField()
    date = models.DateTimeField()
    user = models.ForeignKey(LabUser, on_delete=models.CASCADE)

    def is_latest(self) -> bool:
        return self.experiment.experimentinstructions_set.last() == self

    @property
    def url(self):
        return os.path.join("/", settings.INSTRUCTIONS_URL, self.filename)

    @property
    def relative_url(self):
        return self.url.replace("/" + settings.INSTRUCTIONS_URL, "")

    @property
    def filesize_kib(self):
        return f"{self.filesize / 1024:.0f}"


class Appointment(models.Model):
    class Meta:
        ordering = ["date"]

    date = models.ForeignKey(SemesterDate, on_delete=models.CASCADE)
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    students = models.ManyToManyField(LabUser, related_name="appointments", blank=True)
    is_open = models.BooleanField(default=True)

    def __str__(self):
        return self.date.date.strftime("%Y-%m-%d") + " - " + self.experiment.code

    @staticmethod
    def user_appointments(user: Optional[LabUser], semester: Optional[Semester] = None):
        # if user is admin, just return everything, otherwise filter
        if user is None:
            appointments = Appointment.objects
        else:
            appointments = (
                Appointment.objects.filter(students__in=[user])
                | Appointment.objects.filter(experiment__tutors__in=[user])
                | Appointment.objects.filter(experiment__examiners__in=[user])
            )

        # semester?
        if semester is not None:
            appointments = appointments.filter(date__semester=semester)

        # filter more and return
        return appointments.filter(is_open=True).order_by("date__date")

    @staticmethod
    def tutor_appointments(tutor: LabUser):
        # return appointments with reports of given user and appointments for user's experiments without reports
        return Appointment.objects.filter(
            Q(report__tutor=tutor) | Q(report__tutor__isnull=True, experiment__tutors__in=[tutor])
        ).distinct()

    def num_accepted_reports(self):
        return self.report_set.filter(date__isnull=False).count()

    def close_if_past(self) -> None:
        """Close appointment, if it is in the past."""

        # in the past?
        now = datetime.datetime.now()
        if self.date.date < now.date() or (self.date.date == now.date() and now.hour >= 17):
            # close it
            self.close_appointment()

    @atomic
    def close_appointment(self, students: Optional[List[LabUser]] = None) -> List[Report]:
        # got students?
        if students is None:
            students = self.students.all()

        # loop students of appointment
        reports = []
        for student in students:
            # create report
            report = Report()
            report.student = student
            report.appointment = self
            report.save()
            reports.append(report)

            # store update
            ReportUpdate(report=report, action=UpdateModel.Action.ADDED).save()

        # close appointment
        self.is_open = False
        self.save()

        # store update on appointment
        AppointmentUpdate(appointment=self, action=UpdateModel.Action.CLOSED).save()

        # finish
        return reports


class AppointmentRequest(models.Model):
    class Meta:
        ordering = ["date__date"]

    student = models.ForeignKey(LabUser, on_delete=models.CASCADE)
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    date = models.ForeignKey(SemesterDate, on_delete=models.CASCADE)
    partners = models.ManyToManyField(LabUser, related_name="appointment_requests_partner", blank=True)
    comment = models.CharField(max_length=50, blank=True)

    @staticmethod
    def user_requests(user: Optional[LabUser], semester: Optional[Semester] = None):
        # init
        objs = AppointmentRequest.objects

        # no admin?
        if user is not None:
            # get all requests where user is tutor or student
            objs = objs.filter(Q(student=user) | Q(experiment__tutors=user))

        # semester?
        if semester is not None:
            objs = objs.filter(date__semester=semester)

        # return it
        return objs.all()

    def partners_list(self):
        return ", ".join([p.short_name for p in self.partners.all()])


class Report(models.Model):
    student = models.ForeignKey(LabUser, on_delete=models.CASCADE, related_name="student_reports")
    tutor = models.ForeignKey(
        LabUser, on_delete=models.CASCADE, related_name="tutor_reports", limit_choices_to={"is_tutor": True}, null=True
    )
    appointment = models.ForeignKey(Appointment, on_delete=models.CASCADE)
    date = models.DateTimeField(null=True, blank=True)
    passed = models.BooleanField(null=True)

    def __str__(self):
        return f"[{self.appointment.date.semester.short_name()}] {self.appointment.experiment.code}: {self.student.full_name}"

    def accepts_upload(self, user: Optional[LabUser] = None) -> bool:
        """Returns True, if students can upload new versions of report."""

        # admin can always upload
        if user is not None and user.is_admin:
            return True

        # always return True, if report is still pending
        if self.passed is None:
            return True

        # allow one upload after acceptance
        if self.passed is not None:
            # get number of reports after date
            reports_count = ReportVersion.objects.filter(report=self, date__gte=self.date).count()

            # if none found, allow upload
            if reports_count == 0:
                return True

        # for now, otherwise return False
        return False

    def add_pdf(self, f) -> ReportVersion:
        # get current highest version
        v = self.reportversion_set.aggregate(Max("version"))
        version = v["version__max"] + 1 if v["version__max"] else 1

        # add report version
        rv = ReportVersion()
        rv.report = self
        rv.version = version
        rv.date = datetime.datetime.now()

        # create filename and save
        filename = f"{self.pk}-{self.appointment.experiment.code}-{self.student.last_name}-v{version}.pdf"
        rv.filename = os.path.join(self.appointment.date.date.strftime("%Y"), filename)
        rv.save()

        # create dir
        full_path = os.path.join(settings.REPORTS_ROOT, rv.filename)
        path = os.path.dirname(full_path)
        if not os.path.exists(path):
            os.makedirs(path)

        # save it
        with open(full_path, "wb+") as destination:
            for chunk in f.chunks():
                destination.write(chunk)

        # finished
        return rv

    @property
    def latest(self):
        return self.reportversion_set.filter(report=self).order_by("-version").first()

    @property
    def filename(self):
        # get latest version
        rv = self.latest
        return None if rv is None else rv.filename

    @property
    def report_url(self):
        # get latest version
        rv = self.latest
        return None if rv is None else os.path.join(settings.ROOT_URL, settings.REPORTS_URL, rv.filename)

    @staticmethod
    def pending(user: Optional[LabUser], semester: Optional[Semester] = None):
        if user is None:
            reports = Report.objects.filter(date__isnull=True)
        elif user.is_tutor:
            reports = Report.objects.filter(
                Q(tutor=user) | Q(appointment__experiment__tutors__in=[user]), date__isnull=True
            )
        else:
            reports = Report.objects.filter(student=user, date__isnull=True)

        # semester?
        if semester is not None:
            reports = reports.filter(appointment__date__semester=semester)

        # return
        return reports.distinct().all()

    @staticmethod
    def failed(user: LabUser, semester: Optional[Semester] = None):
        # list
        o = Report.objects.filter(date__isnull=False, passed=False)
        if semester is not None:
            o = o.filter(appointment__date__semester=semester)
        return o if user is None else o.filter(student=user)

    @staticmethod
    def finished(user: Optional[LabUser], semester: Optional[Semester] = None):
        # filter
        o = Report.objects.filter(date__isnull=False, passed=True, exams__isnull=True)

        # semester
        if semester is not None:
            o = o.filter(appointment__date__semester=semester)

        # filter further
        return o if user is None else o.filter(student=user)

    def is_tutor(self, user: LabUser) -> bool:
        return self.tutor == user


class ReportVersion(models.Model):
    class Meta:
        unique_together = (("report", "version"),)

    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    version = models.IntegerField()
    date = models.DateTimeField()
    filename = models.CharField(max_length=100)

    @property
    def report_url(self):
        return os.path.join("/", settings.REPORTS_URL, self.filename)

    @property
    def bare_filename(self):
        return self.filename.split("/")[-1]


class Exam(models.Model):
    student = models.ForeignKey(LabUser, on_delete=models.CASCADE, related_name="student_exams")
    examiner = models.ForeignKey(
        LabUser, on_delete=models.CASCADE, related_name="examiner_exams", limit_choices_to={"is_examiner": True}
    )
    date = models.DateTimeField()
    course = models.IntegerField(choices=[(1, "M.Phy.1401"), (2, "M.Phy.1402")], default=1)
    reports = models.ManyToManyField(Report, related_name="exams")
    active = models.BooleanField(default=True)

    @staticmethod
    def user_exams(user: Optional[LabUser], semester: Optional[Semester] = None):
        # if user is admin, just return everything, otherwise filter
        if user is None:
            exams = Exam.objects
        else:
            exams = Exam.objects.filter(student=user) | Exam.objects.filter(examiner=user)

        # semester?
        if semester is not None:
            exams = exams.filter(date__gte=semester.start, date__lte=semester.end)

        # filter more and return
        return exams.order_by("date__date")


class Email(models.Model):
    subject = models.CharField(max_length=50)
    body = models.TextField()
    recipients = models.ManyToManyField(LabUser, related_name="emails")
    date = models.DateTimeField(auto_now=True)
    to_tutors = models.BooleanField(default=False)
    to_examiners = models.BooleanField(default=False)
    to_semesters = models.ManyToManyField(Semester, related_name="emails")


class UpdateModel(models.Model):
    class Meta:
        ordering = ["date"]

    class Action(str, Enum):
        ADDED = "added"
        CHANGED = "changed"
        CLOSED = "closed"

    date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(LabUser, on_delete=models.CASCADE, null=True)
    action = models.CharField(max_length=10, choices=[(ut, ut) for ut in Action])
    comment = models.CharField(max_length=50, null=True, blank=True)

    def obj(self):
        try:
            return AppointmentUpdate.objects.get(updatemodel_ptr_id=self.pk)
        except AppointmentUpdate.DoesNotExist:
            try:
                return ExamUpdate.objects.get(updatemodel_ptr_id=self.pk)
            except ExamUpdate.DoesNotExist:
                return ReportUpdate.objects.get(updatemodel_ptr_id=self.pk)

    @property
    def subclass(self) -> str:
        return self.obj().__class__.__name__.replace("Update", "")

    @property
    def what(self) -> str:
        return self.obj().what()


class AppointmentUpdate(UpdateModel):
    appointment = models.ForeignKey(Appointment, on_delete=models.CASCADE)

    def what(self) -> str:
        tmp = f"for {self.appointment.experiment.code} " f'on {self.appointment.date.date.strftime("%Y-%m-%d")}'
        if self.action == UpdateModel.Action.ADDED:
            return f"Appointment {tmp} created."
        elif self.action == UpdateModel.Action.CHANGED:
            return f"Appointment {tmp} changed."
        elif self.action == UpdateModel.Action.CLOSED:
            return f"Appointment {tmp} closed."
        else:
            return ""


class ReportUpdate(UpdateModel):
    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    version = models.ForeignKey(ReportVersion, on_delete=models.CASCADE, null=True)

    def what(self) -> str:
        tmp = (
            f"for {self.report.appointment.experiment.code} "
            f'on {self.report.appointment.date.date.strftime("%Y-%m-%d")}'
        )
        if self.action == UpdateModel.Action.ADDED:
            return f"Report {tmp} created."
        elif self.action == UpdateModel.Action.CHANGED:
            return f'<a href="{self.version.report_url}"> Version {self.version.version}</a> {tmp} uploaded.'
        elif self.action == UpdateModel.Action.CLOSED:
            return f"Report {tmp} {'passed' if self.report.passed else 'failed'}."
        else:
            return ""


class ExamUpdate(UpdateModel):
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)

    def what(self) -> str:
        return ""


class PartnerSearch(models.Model):
    user = models.ForeignKey(LabUser, on_delete=models.CASCADE)
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)


class Content(models.Model):
    key = models.CharField(max_length=20)
    value = models.TextField()


class SafetyBriefing(models.Model):
    date = models.DateField(unique=True)
    students = models.ManyToManyField(LabUser, related_name="safety_briefings", blank=True)


class LabEvaluationRequest(models.Model):
    date = models.DateTimeField(auto_now=True)
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
    uuid = models.CharField(max_length=36)


class LabEvaluation(models.Model):
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
    duration_lab = models.SmallIntegerField(validators=[MinValueValidator(0), MaxValueValidator(50)])
    duration_report = models.SmallIntegerField(validators=[MinValueValidator(0), MaxValueValidator(50)])
    grade_instructions = models.SmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(6)])
    grade_setup = models.SmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(6)])
    grade_learned = models.SmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(6)])
    grade_interesting = models.SmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(6)])
    grade_overall = models.SmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(6)])
    comments = models.TextField(max_length=1000, blank=True, null=True)
