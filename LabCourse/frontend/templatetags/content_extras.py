from django import template
from django.utils.html import format_html

from LabCourse.api.models import Content

register = template.Library()


@register.simple_tag
def content(key):
    # get content
    try:
        c = Content.objects.get(key=key)
    except Content.DoesNotExist:
        return None

    # return content
    return format_html(c.value)
