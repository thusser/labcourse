from django import template
from django.utils.safestring import mark_safe


register = template.Library()


@register.filter()
def email(value, user):
    if user.is_authenticated:
        return mark_safe(f'<a href="mailto:{value}">{value}</a>')
    else:
        return mark_safe(value.replace("@", "&nbsp;&lt;at&gt;&nbsp;"))
