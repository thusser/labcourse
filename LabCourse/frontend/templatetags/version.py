from django import template


register = template.Library()


@register.simple_tag
def version():
    import LabCourse

    return LabCourse.__version__
