import logging
from django.core.mail import send_mail

from LabCourse import settings
from LabCourse.api.models import LabUser, LabEvaluationRequest, LabEvaluation


def _send_email_evaluation_request(recipient: LabUser, eval_request: LabEvaluationRequest):
    # build body
    body = f"""\
Dear {recipient.full_name},

congratulations for finishing the {eval_request.experiment.code} lab in the Advanced Lab Course!

Please take a minute or two to fill an evaluation form:
{settings.ROOT_URL}/evaluation/{eval_request.uuid}/

The data you provide is kept completely anonymous, only what you see in the form will be stored. 
Please provide a faithful judgement for your experience, which will be used to improve the individual 
lab and the lab course in general.

Regards,
Advanced Lab Course    
"""

    # send it
    logging.info(f"Sending lab eval request to {recipient.email}.")
    send_mail(
        f"[ALC] Lab evaluation request for {eval_request.experiment.code}",
        body,
        settings.EMAIL_FROM,
        [recipient.email],
    )


def send_email_evaluation_request(eval_request: LabEvaluationRequest, student: LabUser):
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_EVALUATION_REQUEST"]:
        _send_email_evaluation_request(student, eval_request)
    if settings.LABCOURSE["ADMIN"]["SEND_EMAIL_EVALUATION_REQUEST"]:
        for admin in LabUser.objects.filter(is_admin=True):
            _send_email_evaluation_request(admin, eval_request)


def _send_email_evaluation(recipient: LabUser, evaluation: LabEvaluation):
    # create grades
    grade_instructions = "☆" * evaluation.grade_instructions
    grade_setup = "☆" * evaluation.grade_setup
    grade_learned = "☆" * evaluation.grade_learned
    grade_interesting = "☆" * evaluation.grade_interesting
    grade_overall = "☆" * evaluation.grade_overall

    # build body
    body = f"""\
Dear {recipient.full_name},

an evaluation for {evaluation.experiment.code} in {evaluation.semester} has just been sent.

Duration lab:        {evaluation.duration_lab} hours
Duration report:     {evaluation.duration_report} hours

Instructions:        {grade_instructions}
Setup and Execution: {grade_setup}

I learned something: {grade_learned}
It was interesting:  {grade_interesting}

Overall:             {grade_overall}

Comments:
{evaluation.comments}
    
Regards,
Advanced Lab Course    
"""

    # send it
    logging.info(f"Sending lab evaluation to {recipient.email}.")
    send_mail(
        f"[ALC] Lab evaluation for {evaluation.experiment.code}",
        body,
        settings.EMAIL_FROM,
        [recipient.email],
    )


def send_email_evaluation(evaluation: LabEvaluation):
    if settings.LABCOURSE["ADMIN"]["SEND_EMAIL_EVALUATION"]:
        for admin in LabUser.objects.filter(is_admin=True):
            _send_email_evaluation(admin, evaluation)
