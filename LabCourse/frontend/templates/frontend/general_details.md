# Registration
You register (as usual) via FlexNow for the module M.phy.1401 (part 1) that requires four labs. If you have already 
finished that one you can register for M.phy.1402 (part 2) for another four labs. Accepted reports for the four labs 
in each module are the prerequisite to register for the final exam.

After registering, you can sign up for a lab, actually perform it, and then write a report, which has to be accepted 
by the tutor of the experiment. After you've done that for four labs, you can register for the final exam.

Please register (or unregister, if applicable) by the due date. Timely registration is mandatory for the assignment of 
grades! Subsequent registration is not possible.

# Module exam
After performing four different labs and having an accepted report for each of them, you can request an appointment for 
the exam at one of the examiners. Note that you should pick an examiner that is close to the topic of your labs, so 
after performing, e.g., four labs in the field of astrophysics, you should choose your examiner accordingly.

The exam itself will be an oral exam of 30 minutes about those four labs: theory of the experiment, execution of the 
lab, and results. 

After the exam the examiner will grade your performance and submit it to FlexNow.

# Lab course groups
The experiments of the lab course are carried out in groups of two. It is therefore advisable to look for a lab 
partner(s) before booking the experiment. You can specify a lab partner when booking, but each student has to register 
by themself. You might also request a lab for yourself only, in which case you might be paired with another student by 
the tutor. Upon request (to the supervisor or organizer), it is also possible to form a group of three. However, 
this should remain an exception.

# Booking of labs
The booking of labs is done via this website. Before this function can be used, a registration for the event in stud.ip 
is required. Attention: this registration only refers to the booking. It is independent of the registration for the 
module exam via FlexNow and does not replace it!

A detailed description and further information can be found on the MyLab page.\r\n\r\nThe booking of labs starts on the 
day of the preliminary discussion of the lab course and is possible until the end of the lecture period.

# Contact the tutors
In good time (i.e. at least one week) before the lab is to be carried out, please contact the responsible tutor. During 
the discussion with the tutor, you may receive literature folders or literature references, information on the 
experimental procedure, and you can ask questions in advance or arrange for possible postponements.

# Instructions and lab preparation
The lab instructions are available for download on this website (Downloads or Experiments). You are expected to obtain 
and thoroughly review the appropriate instructions at least 1 week before conducting a lab. This includes answering 
the questions posed in the instructions and reading secondary literature. It is not possible to carry out a lab without 
sufficient preparation!

It has already been pointed out that you are expected to contact the tutor in good time 
before the date of the lab.

# Lab execution
The location of the lab is indicated for each experiment. Please be there punctually on the day of the lab. Before the 
start of the experimental work, there usually is a discussion with the tutor, in which you can discuss the most 
important points of the lab, receive practical tips and also ask questions. In order to avoid damage to equipment, 
the tutor should be consulted in case of doubt (before switching on!). In the case of safety-relevant procedures 
(laser or radioactive radiation, high voltage, etc.) and when using highly sensitive and expensive equipment, the 
presence of the tutor(s) is required! Please clean up after the lab and dismantle the experiment if necessary. Please 
report faulty/defective devices or experimental equipment to your tutor immediately.

# Logging, lab book
It is important to record the entire experimental procedure. For this purpose, it is advisable to keep a lab notebook. 
All experimental steps, measurement data, relevant instrument settings and environmental conditions should be documented 
in it (it is better to write down too much than too little!). This transcript (then in the form of photocopies of the 
respective lab book pages) forms an essential part of the experimental protocol. If data are entered directly into the 
laptop, a printout of the data/tables is also part of the protocol. Check your data already during the measurement for 
sense, consistency and completeness, e.g. by means of a provisionally created graph. In this way, you can detect errors 
or discrepancies in good time.

# Written report - 1. general
The written paper (the 'lab report' in the broader sense) summarizes your work on the lab and the results obtained and 
documents your performance in an assessable form. The report should be written and formulated independently by each 
participant. The evaluation of the measurement data can be done in groups, i.e. in a team, but should be presented in 
each case in your own words. The independence of the work is confirmed by the signature on the cover sheet. The length 
of the paper should not significantly exceed 10 text pages (not counting pictures and protocol).

# Written report - 2. structure
The report consists of the following parts:

- Standard cover sheet with all required information, with signature (pdf file template).
- Summary description of the experiment, experimental objective (approx. 1 page).
- Concise theory part - only the essentials, possibly answering the questions asked in the instructions (3-4 pages).
- Evaluation and discussion of the results, including error calculation and discussion! (5-6 pages).
- Lab protocol (original or photocopy)
- The steps of execution should be evident from the protocol and do not have to be presented separately.

# Written report - 3. formal aspects
The following formal requirements should be adhered to:

- It is recommended to use a word processing program, preferably LaTeX (templates).
- Please use only the standard cover sheet.
- Staple sheets firmly together (preferably cardboard stapling strips, please no plastic staplers and no ring binding!).
- Structuring of the text, clear layout.
- Diagrams sufficiently large, axes legibly labeled and appropriately subdivided.
- Sensible rounding of numerical values, error information, SI units.
- Correct spelling and punctuation as far as possible (read again before handing in!).
- Keyword sustainability: the use of recycled paper and the renunciation of and the use of plastic bindings or staplers is recommended.
- Paper quality does not affect the grade!

# Deadline
The reports are handed in personally to the respective tutor (by arrangement). The deadline for submission ends 2 weeks 
after the lab has been carried out. After that, the lab is considered failed. An extension of the deadline is possible 
in justified cases (e.g. illness). The report will only be accepted if it has a correctly completed and signed cover 
sheet, is firmly stapled together and contains the measurement protocol with all measurement data in the original or 
as a photocopy. The tutor reviews the report and, if necessary, returns it to the student for corrections. When the 
report is good enough, the tutor will accept it

# Routing slip
You can (but don't have to) keep a routing slip (pdf file). For each lab, the tutor certifies the execution and 
submission of the work with date and signature, the examiner enters the achieved score with signature. The routing 
slip is not absolutely necessary for the organizational process. It serves as an additional proof of your personal 
performance for the hopefully not occurring case that elaborations or stored data get lost.

# Special dates
Some labs can only be carried out in one block or not at the regular lab course time due to technical circumstances or 
intensive use of the equipment. For these labs, please contact the tutor of the experiment. They can book a lab for you 
at any date.

# Lab language English
Some labs in the ALC are supervised by pure English speakers. This means that you will speak with the tutor in English 
and also write your report in this language. The language of the tutor is indicated for each lab. If you do not feel 
sufficiently confident in English, you can choose from a sufficient number of labs held in German. Under certain 
circumstances, you can also agree on a report in English with German-speaking tutors in order to consolidate your 
knowledge of this scientific language.

# Safety instructions
No lab may be carried out in the ALC without prior safety instruction. All important information on work safety (laser 
protection, radiation protection, etc.) is summarized in a leaflet (pdf file). These points will also be covered again 
in a safety briefing following the preliminary ALC meeting. You are requested to confirm that you have read and 
understood this information by signing the form (pdf file) and handing it in to the organizer after the preliminary 
meeting/instruction. Only after receipt of the signed form your participation in the AC is possible!

# Crediting of external achievements (labs abroad, courses, etc.)
In principle, it is possible to have certain achievements credited to the ALC. This applies, for example, to summer 
schools, vacation internships, internships abroad, research stays, and the like. The prerequisite for the crediting is 
that you do not wish to include these undertakings elsewhere as part of your course work and that the work carried out 
is mainly of an experimental nature.

The decision on the type and extent of the credit will be made on a case-by-case basis. It is usually necessary to ask 
a lecturer of the faculty for an opinion on the extent of the credit including the grading of your external work.

It is requested that such a project be discussed with the ALC organizer at an early stage.
