from django import forms

from LabCourse.api.models import LabEvaluation, LabEvaluationRequest, Experiment, Semester


class LabEvaluationForm(forms.ModelForm):
    class Meta:
        model = LabEvaluation
        exclude = ["experiment", "semester"]

    def __init__(self, eval_request: LabEvaluationRequest, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.eval_request = eval_request
