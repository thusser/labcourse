import os
from typing import Optional, Any, Dict

from django.db.transaction import atomic
from django.http import Http404
from django.shortcuts import render
from django.views.generic import TemplateView, ListView, FormView

from LabCourse.api.models import (
    Category,
    Experiment,
    Semester,
    Appointment,
    LabUser,
    PartnerSearch,
    LabEvaluationRequest,
    LabEvaluation,
)
from LabCourse.frontend.email import send_email_evaluation
from LabCourse.frontend.forms import LabEvaluationForm


class GeneralInfoView(TemplateView):
    template_name = "frontend/general.html"

    def get_context_data(self, **kwargs):
        # load data
        with open("LabCourse/frontend/templates/frontend/general_details.md") as f:
            # split by topics
            data = ["#" + block for block in f.read().split("#")][1:]

        # now split title
        topics = []
        for d in data:
            # split lines
            s = d.split("\n")

            # get title and body
            topics.append({"title": s[0][1:].strip(), "body": "\n".join(s[1:]).strip()})

        # set context
        context = super().get_context_data(**kwargs)
        context["topics"] = topics
        return context


class ExperimentsView(TemplateView):
    template_name = "frontend/experiments.html"

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    @property
    def category(self) -> Optional[str]:
        try:
            return self.kwargs["category"]
        except KeyError:
            return None

    @property
    def experiment(self) -> Optional[str]:
        try:
            return self.kwargs["experiment"]
        except KeyError:
            return None

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)

        # get all categories
        context["categories"] = Category.objects.order_by("code").all()

        # if category is selected, get experiments
        if self.category is not None:
            cat = Category.objects.get(code=self.category.upper())
            context["category"] = cat
            context["experiments"] = cat.experiment_set.all()

        # if experiment is selected, get info
        if self.experiment is not None:
            # get experiment
            exp = Experiment.objects.get(code=self.category.upper() + "." + self.experiment.upper())
            context["experiment"] = exp

            # get available dates
            sem = Semester.current()
            context["semester"] = sem
            tmp = context["semester"].semesterdate_set.filter(special=False).all()
            context["dates"] = {date: Appointment.objects.filter(experiment=exp, date=date).exists() for date in tmp}

            # authenticated?
            if self.request.user.is_authenticated:
                # post data?
                if "add_partner_search" in self.request.POST:
                    PartnerSearch.objects.get_or_create(user=self.request.user, experiment=exp, semester=sem)
                if "del_partner_search" in self.request.POST:
                    PartnerSearch.objects.filter(user=self.request.user, experiment=exp, semester=sem).delete()

                # get partner searches
                context["partner_searches"] = PartnerSearch.objects.filter(experiment=exp, semester=sem)
                context["student_searching"] = PartnerSearch.objects.filter(
                    experiment=exp, semester=sem, user=self.request.user
                ).exists()

                # is user tutor for current project?
                context["is_tutor"] = exp.is_tutor(self.request.user)

        return context


class TutorListView(ListView):
    template_name = "frontend/tutors.html"
    queryset = LabUser.objects.filter(is_tutor=True).order_by("last_name", "first_name")
    context_object_name = "tutors"


class ExaminerListView(ListView):
    template_name = "frontend/examiners.html"
    queryset = LabUser.objects.filter(is_examiner=True).order_by("last_name", "first_name").all()
    context_object_name = "examiners"


@atomic
def evaluate_lab(request, uuid):
    # get request
    try:
        eval_request = LabEvaluationRequest.objects.get(uuid=uuid)
    except LabEvaluationRequest.DoesNotExist:
        raise Http404

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = LabEvaluationForm(eval_request, request.POST)

        # check whether it's valid:
        if form.is_valid():
            # create an evaluation
            evaluation = LabEvaluation()

            # fill it
            evaluation.experiment = eval_request.experiment
            evaluation.semester = eval_request.semester
            evaluation.duration_lab = form.cleaned_data["duration_lab"]
            evaluation.duration_report = form.cleaned_data["duration_report"]
            evaluation.grade_instructions = form.cleaned_data["grade_instructions"]
            evaluation.grade_setup = form.cleaned_data["grade_setup"]
            evaluation.grade_learned = form.cleaned_data["grade_learned"]
            evaluation.grade_interesting = form.cleaned_data["grade_interesting"]
            evaluation.grade_overall = form.cleaned_data["grade_overall"]
            evaluation.comments = form.cleaned_data["comments"]
            evaluation.save()

            # remove request
            eval_request.delete()

            # send email
            send_email_evaluation(evaluation)

            # finished
            return render(request, "frontend/labeval.html", {"success": True})

    else:
        # create form
        form = LabEvaluationForm(eval_request=eval_request)

    # render
    return render(request, "frontend/labeval.html", {"form": form})
