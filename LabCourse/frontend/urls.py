"""LabCourse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os
import urllib.parse

from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, Http404
from django.urls import path, re_path, include
from django.views.generic import TemplateView, RedirectView
from django.views.static import serve

from .views import (
    ExperimentsView,
    TutorListView,
    ExaminerListView,
    GeneralInfoView,
    evaluate_lab,
)
from .. import settings
from ..api.models import Report, LabUser, ExperimentInstructions, Experiment


@login_required
def protected_media(request, path):
    response = HttpResponse(status=200)
    response["Content-Type"] = ""
    # nginx needs to be configured to serve /media/private from /media_private
    response["X-Accel-Redirect"] = "/media_private/" + path
    return response


def _report_id(path: str) -> int:
    # get filename
    if "/" not in path:
        raise Http404()
    filename = path.split("/")[1]

    # get report id
    if "-" not in filename:
        raise Http404()
    s = filename.split("-")[0]

    # to int
    try:
        report_id = int(s)
        if str(report_id) != s:
            raise Http404()
        return report_id
    except ValueError:
        raise Http404()


def _is_user_allowed_report(report: Report, user: LabUser):
    # most be one of:
    # 1. student who wrote the report
    is_student = user == report.student
    # 2. admin
    # 3. tutor
    is_tutor = (user == report.tutor) or (user in report.appointment.experiment.tutors.all())
    # 4. examiner, if exam exists
    is_examiner = any([user.is_user_or_delegate(exam.examiner) for exam in report.exams.all()])
    return is_student or user.is_admin or is_tutor or is_examiner


@login_required
def protected_report(request, path):
    # get report
    report = Report.objects.get(pk=_report_id(urllib.parse.unquote(path)))

    # user must be student, examiner or admin
    if not _is_user_allowed_report(report, request.user):
        raise PermissionDenied()

    # all seems to be okay, serve file
    response = HttpResponse(status=200)
    response["Content-Type"] = ""
    # nginx needs to be configured to serve /reports from /reports
    response["X-Accel-Redirect"] = "/reports_private/" + urllib.parse.quote(path)
    return response


def _instructions(path: str) -> ExperimentInstructions:
    # get filename
    if "/" not in path:
        raise Http404()
    filename = path.split("/")[1]

    # get experiment code and version
    # pattern is <code>(?-v<version>).pdf, i.e. ag.den.pdf or ag.den-v3.pdf
    if "." not in filename:
        raise Http404()
    filename_no_ext = os.path.splitext(filename)[0]
    s = filename_no_ext.split("-v")
    if len(s) == 1:
        code, version = s[0], None
    elif len(s) == 2:
        code, version = s[0], int(s[1])
    else:
        raise Http404()

    # get experiment
    try:
        experiment = Experiment.objects.get(code=code.upper())
    except Experiment.DoesNotExist:
        raise Http404()

    # get latest instructions
    if version:
        instructions = experiment.experimentinstructions_set.filter(version=version).first()
    else:
        instructions = experiment.experimentinstructions_set.last()
    if instructions is None:
        raise Http404()
    return instructions


def _is_user_allowed_instructions(instructions: ExperimentInstructions, user: LabUser):
    # most be one of:
    return (
        # 1. admin
        user.is_admin
        # 2. tutor of lab
        or instructions.experiment.is_tutor(user)
        # 3. student now and instructions is latest version
        or (user.is_authenticated and instructions.is_latest())
    )


@login_required
def protected_instructions(request, path):
    # get instructions
    instructions = _instructions(path)

    # user must be tutor, examiner or admin
    if not _is_user_allowed_instructions(instructions, request.user):
        raise PermissionDenied()

    # all seems to be okay, serve file
    response = HttpResponse(status=200)
    response["Content-Type"] = ""
    # nginx needs to be configured to serve /reports from /instructions
    response["X-Accel-Redirect"] = "/instructions_private/" + instructions.relative_url
    return response


@login_required
def serve_private_media(request, path, document_root=None, show_indexes=False):
    return serve(request, path, document_root=document_root, show_indexes=show_indexes)


@login_required
def serve_report(request, path, document_root=None, show_indexes=False):
    # get report
    report = Report.objects.get(pk=_report_id(path))

    # user must be student, examiner or admin
    if not _is_user_allowed_report(report, request.user):
        raise PermissionDenied()

    # all seems to be okay, serve file
    return serve(request, path, document_root=document_root, show_indexes=show_indexes)


@login_required
def serve_instructions(request, path, document_root=None, show_indexes=False):
    # get instructions
    instructions = _instructions(path)

    # user must be student, examiner or admin
    if not _is_user_allowed_instructions(instructions, request.user):
        raise PermissionDenied()

    # all seems to be okay, serve file
    return serve(request, instructions.relative_url, document_root=document_root, show_indexes=show_indexes)


favicon_view = RedirectView.as_view(url="/static/favicon-32x32.ico", permanent=True)
robots_view = TemplateView.as_view(template_name="robots.txt", content_type="text/plain")

urlpatterns = [
    path("", TemplateView.as_view(template_name="frontend/home.html"), name="home"),
    path("general/", GeneralInfoView.as_view()),
    path("experiments/", ExperimentsView.as_view()),
    path("experiments/<str:category>/", ExperimentsView.as_view(), name="category"),
    path("experiments/<str:category>/<str:experiment>/", ExperimentsView.as_view(), name="experiment"),
    path("tutors/", TutorListView.as_view(), name="tutors"),
    path("examiners/", ExaminerListView.as_view(), name="examiners"),
    path("downloads/", TemplateView.as_view(template_name="frontend/downloads.html")),
    path("contact/", TemplateView.as_view(template_name="frontend/contact.html")),
    path("mylab/", include("LabCourse.mylab.urls"), name="mylab"),
    path("evaluation/<str:uuid>/", evaluate_lab, name="labeval"),
    re_path(r"^favicon\.ico$", favicon_view),
    path("robots.txt", robots_view),
]

# in DEBUG setting, we also serve media files
if settings.SERVE_MEDIA:
    # media
    urlpatterns.append(
        re_path(
            r"^private/(?P<path>.*)$",
            serve_private_media,
            kwargs={"document_root": settings.PRIVATE_MEDIA_ROOT},
        )
    )
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    # reports
    urlpatterns.append(
        re_path(
            r"^reports/(?P<path>.*)$",
            serve_report,
            kwargs={"document_root": settings.REPORTS_ROOT},
        )
    )

    # instructions
    urlpatterns.append(
        re_path(
            r"^instructions/(?P<path>.*)$",
            serve_instructions,
            kwargs={"document_root": settings.INSTRUCTIONS_ROOT},
        )
    )

else:
    urlpatterns.append(re_path(r"^private/(?P<path>.*)", protected_media, name="protect_media"))
    urlpatterns.append(re_path(r"^reports/(?P<path>.*)", protected_report, name="protect_report"))
    urlpatterns.append(re_path(r"^instructions/(?P<path>.*)", protected_instructions, name="protect_instructions"))
