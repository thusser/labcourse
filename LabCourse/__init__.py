from .celery import app as celery_app
from pathlib import Path
from single_source import get_version


__version__ = get_version(__name__, Path(__file__).parent.parent)


__all__ = ["celery_app"]
