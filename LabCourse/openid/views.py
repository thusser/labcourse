import logging
from urllib.parse import urlencode

from authlib.integrations.base_client import OAuthError
from authlib.integrations.django_client import OAuth
from authlib.oauth2.rfc6749 import OAuth2Token
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.contrib import auth
import urllib.parse


# create OAth client
from LabCourse import settings
from LabCourse.api.models import LabUser


def client():
    oauth = OAuth()
    oauth.register("gwdg", scope="openid profile goesternMatrikelnummer org goeId email")
    return oauth.create_client("gwdg")


def login(request):
    # build a full authorize callback uri
    redirect_uri = request.build_absolute_uri("/openid/authorize").replace("http", "https")

    # next?
    if "next" in request.GET:
        redirect_uri += "?next=" + request.GET["next"]

    # redirect
    return client().authorize_redirect(request, redirect_uri)


def logout(request):
    # logout
    auth.logout(request)

    # build url
    gwdg = client()
    if gwdg and "end_session_endpoint" in gwdg.server_metadata:
        query_string = urlencode({"redirect_uri": settings.LOGOUT_REDIRECT_URL})
        url = f"{gwdg.server_metadata['end_session_endpoint']}?{query_string}"
        return redirect(url)
    else:
        return redirect("/")


def authorize(request):
    # get token and user
    gwdg = client()
    token = gwdg.authorize_access_token(request)

    # get userinfo
    try:
        res = gwdg.get(gwdg.server_metadata["userinfo_endpoint"], token=OAuth2Token(token))
        if not res.ok:
            return HttpResponse("Unauthorized", status=401)
        userinfo = res.json()

    except OAuthError:
        return HttpResponse("Unauthorized", status=401)

    # got a student number?
    student_number = int(userinfo["goesternMatrikelnummer"]) if "goesternMatrikelnummer" in userinfo else None

    # get user
    user, _ = LabUser.objects.get_or_create(
        username=userinfo["uid"],
        defaults={
            "first_name": userinfo["given_name"],
            "last_name": userinfo["family_name"],
            "email": userinfo["email"],
            "student_number": student_number,
        },
    )

    # log in
    auth.login(request, user)

    # next?
    if "next" in request.GET:
        return redirect(urllib.parse.unquote(request.GET["next"]))
    else:
        return redirect("/")
