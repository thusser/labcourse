from typing import Any

from django import forms

from LabCourse.api.models import Semester


class SendEmailsForm(forms.Form):
    to_tutors = forms.BooleanField(label="Tutors", required=False)
    to_examiners = forms.BooleanField(label="Examiners", required=False)
    to_students_semesters = forms.MultipleChoiceField(required=False)
    subject = forms.CharField(label="Subject", required=True, max_length=50)
    body = forms.CharField(label="Body", required=True, widget=forms.Textarea(attrs={"rows": 10}))

    def __init__(self, *args: Any, **kwargs: Any):
        forms.Form.__init__(self, *args, **kwargs)
        self.fields["to_students_semesters"].choices = ((s.code, str(s)) for s in Semester.objects.all())


class ImportStudipForm(forms.Form):
    csv = forms.FileField(label="StudIP CSV", required=True)
