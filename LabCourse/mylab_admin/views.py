from typing import Optional

from django.contrib.auth.decorators import user_passes_test
from django.core.mail import send_mail
from django.db.transaction import atomic
from django.http import Http404
from django.shortcuts import render, redirect

from LabCourse.api.management.commands.import_studip import import_from_studip
from LabCourse.api.models import LabUser, Semester, JoinRequest, Appointment, Experiment, Category, Report, Email
from LabCourse.mylab.email import send_email_join_rejected, send_email_join_accepted
from LabCourse.mylab.views import get_user_home_data
from LabCourse.mylab_admin.forms import SendEmailsForm, ImportStudipForm


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def admin(request):
    # get data
    data = {
        "mylab_page": "User",
    }
    if request.user.is_authenticated:
        data.update(**get_user_home_data(None))

    return render(request, "mylab_admin/admin.html", data)


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def semesters(request):
    # get all semesters
    semesters = Semester.objects.order_by("lecture_start").all()
    return render(request, "mylab_admin/semesters.html", {"mylab_page": "Admin", "semesters": semesters})


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def semester_current(request):
    semester = Semester.current().safe_code
    return redirect("mylab.admin.semester", semester)


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def semester_details(request, semester: str):
    # get semester
    semester = Semester.objects.get(code=semester.replace("-", "/"))

    # get data
    data = get_user_home_data(None, semester)

    # get appointments and labs
    appointments = Appointment.objects.filter(date__semester=semester).distinct()
    experiments = Experiment.objects.filter(appointment__date__semester=semester).distinct()
    students = LabUser.objects.filter(appointments__date__semester=semester).distinct()

    # render
    return render(
        request,
        "mylab_admin/semester_details.html",
        {
            "mylab_page": "Admin",
            "semester": semester,
            "appointments": appointments,
            "experiments": experiments,
            "students": students,
            **data,
        },
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def semester_overview(request, semester: str):
    # get semester
    sem = Semester.objects.get(code=semester.replace("-", "/"))

    # get initial data
    data = {"semester": sem, "categories": Category.objects.order_by("code").all()}

    # init appointments
    apps = Appointment.objects.filter(date__semester=sem).order_by("date__date", "experiment__code")

    # get appointments and students
    data["appointments"] = apps
    data["appointments_valid"] = apps.exclude(students=None)
    data["student_labs"] = LabUser.objects.filter(appointments__in=apps)
    data["students"] = data["student_labs"].distinct()
    data["student_experiments"] = Experiment.objects.filter(appointment__in=apps).distinct()
    reports = Report.objects.filter(appointment__in=apps)
    data["tutors"] = LabUser.objects.filter(tutor_reports__in=reports).distinct()

    # render
    return render(
        request,
        "mylab_admin/semester_overview.html",
        {"mylab_page": "Admin", **data},
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def students(request, semester):
    # get semester
    semesters = Semester.objects.filter(code=semester.replace("-", "/"))
    if semesters.count() == 0:
        raise Http404()
    semester = semesters.first()

    # get students
    students = semester.students.order_by("last_name", "first_name")

    # render
    return render(
        request, "mylab_admin/students.html", {"mylab_page": "Admin", "semester": semester, "students": students}
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def students_current(request):
    semester = Semester.current().safe_code
    return redirect("mylab.admin.semester.students", semester)


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def user_details(request, user_id):
    # get user
    labuser = LabUser.objects.get(pk=user_id)

    # get data
    data = get_user_home_data(labuser)
    data["mylab_page"] = "Admin"
    data["admin"] = True

    return render(request, "mylab_admin/user.html", data)


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def user_details_as_user(request, user_id):
    # get user
    labuser = LabUser.objects.get(pk=user_id)

    # get data
    data = get_user_home_data(labuser)
    data["mylab_page"] = "Admin"

    return render(request, "mylab_admin/user.html", data)


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def tutors(request):
    # render
    return render(
        request,
        "mylab_admin/tutors.html",
        {"mylab_page": "Admin", "tutors": LabUser.objects.filter(is_tutor=True).order_by("last_name", "first_name")},
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def examiners(request):
    # render
    return render(
        request,
        "mylab_admin/examiners.html",
        {
            "mylab_page": "Admin",
            "examiners": LabUser.objects.filter(is_examiner=True).order_by("last_name", "first_name"),
        },
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def accept_join(request, user_id):
    # get user and current semester
    try:
        user = LabUser.objects.get(pk=user_id)
    except LabUser.DoesNotExist:
        raise Http404()
    semester = Semester.current()

    # add semester for user
    user.student_semesters.add(semester)

    # delete all requests for user
    JoinRequest.objects.filter(user=user).delete()

    # send email
    send_email_join_accepted(user)

    # redirect to mylab
    return redirect("mylab")


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def reject_join(request, user_id):
    # get user
    try:
        user = LabUser.objects.get(pk=user_id)
    except LabUser.DoesNotExist:
        raise Http404()

    # delete all requests for user
    JoinRequest.objects.filter(user=user).delete()

    # send email
    send_email_join_rejected(user)

    # redirect to mylab
    return redirect("mylab")


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def send_emails(request):
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = SendEmailsForm(request.POST)

        # valid form?
        if form.is_valid():
            # get subject and body
            subject = "[ALC] " + form.cleaned_data["subject"]
            body = form.cleaned_data["body"]

            # admins are always recipients
            users = LabUser.objects.filter(is_admin=True).all()

            # init email
            email = Email()
            email.subject = subject
            email.body = body

            # tutors?
            if form.cleaned_data["to_tutors"]:
                email.to_tutors = True
                users |= LabUser.objects.filter(is_tutor=True).all()

            # examiners?
            if form.cleaned_data["to_examiners"]:
                email.to_examiners = True
                users |= LabUser.objects.filter(is_examiner=True).all()

            # save email
            email.save()

            # students
            if form.cleaned_data["to_students_semesters"]:
                # loop semester codes
                for code in form.cleaned_data["to_students_semesters"]:
                    # get semester
                    semester = Semester.objects.get(code=code)

                    # add semester
                    email.to_semesters.add(semester)

                    # add all students
                    users |= semester.students.all()

            # set all recipients
            email.recipients.set(users)

            # get email adresses
            recipients = [f"{u.full_name} <{u.email}>" for u in users]

            # loop recipients
            for recipient in recipients:
                # send email
                send_mail(
                    subject,
                    body,
                    "alc.physics@uni-goettingen.de",
                    [recipient],
                )

            # render
            return render(
                request,
                "mylab_admin/send_emails.html",
                {"mylab_page": "Admin", "success": True},
            )

    else:
        # create form
        form = SendEmailsForm()

    # render
    return render(
        request,
        "mylab_admin/send_emails.html",
        {"mylab_page": "Admin", "form": form},
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def list_emails(request):
    # get emails
    emails = Email.objects.order_by("date").all()

    # render
    return render(
        request,
        "mylab_admin/list_emails.html",
        {"mylab_page": "Admin", "emails": emails},
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def email_details(request, email_id: int):
    # get email
    email = Email.objects.get(pk=email_id)

    # render
    return render(
        request,
        "mylab_admin/email_details.html",
        {"mylab_page": "Admin", "email": email},
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
def search_user(request):
    # render
    return render(
        request,
        "mylab_admin/search_user.html",
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
@atomic
def import_studip(request):
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = ImportStudipForm(request.POST, request.FILES)

        # check whether it's valid:
        if form.is_valid():
            # import it
            result = import_from_studip(request.FILES["csv"])

            # render
            return render(
                request,
                "mylab_admin/import_studip.html",
                {"success": True, "result": result},
            )

    else:
        # create form
        form = ImportStudipForm()

    # render
    return render(
        request,
        "mylab_admin/import_studip.html",
        {"form": form},
    )


@user_passes_test(lambda u: u.is_authenticated and u.is_admin)
@atomic
def appointment(request, appointment_id):
    # get appointment
    appointment = Appointment.objects.get(pk=appointment_id)

    # render
    return render(
        request,
        "mylab_admin/appointment.html",
        {"appointment": appointment},
    )
