from django import template
import pandas as pd
import plotly.express as px
from django.db.models import Count, F
from django.utils.safestring import mark_safe

from LabCourse.api.models import Semester, Appointment

register = template.Library()


@register.simple_tag
def semester_experiments_plot(semester: Semester):
    # get data
    data = pd.DataFrame(
        Appointment.objects.filter(date__semester=semester)
        .annotate(Experiment=F("experiment__code"))
        .annotate(Students=Count("students"))
        .values("Experiment", "Students")
        .all()
    )

    if len(data) > 0:
        # let's do the grouping per experiment in pandas
        data = data.groupby(["Experiment"]).sum().reset_index()

        # order by number of students
        data = data.sort_values("Students", ascending=False)

        # plot and return
        fig = px.bar(data, x="Experiment", y="Students")
        return mark_safe(fig.to_html(full_html=False))

    else:
        return ""
