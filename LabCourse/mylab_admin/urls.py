"""LabCourse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from .views import (
    admin,
    students,
    students_current,
    tutors,
    examiners,
    user_details,
    user_details_as_user,
    accept_join,
    reject_join,
    semesters,
    semester_current,
    semester_details,
    semester_overview,
    send_emails,
    list_emails,
    email_details,
    search_user,
    import_studip,
    appointment,
)

urlpatterns = [
    path("", admin, name="mylab.admin"),
    path("semesters/", semesters, name="mylab.admin.semesters"),
    path("semester/current/", semester_current, name="mylab.admin.semester.current"),
    path("semester/current/students/", students_current, name="mylab.admin.semester.current.students"),
    path("semester/<str:semester>/", semester_overview, name="mylab.admin.semester"),
    path("semester/<str:semester>/details/", semester_details, name="mylab.admin.semester.details"),
    path("semester/<str:semester>/students/", students, name="mylab.admin.semester.students"),
    path("tutors/", tutors, name="mylab.admin.tutors"),
    path("examiners/", examiners, name="mylab.admin.examiners"),
    path("user/", search_user, name="mylab.admin.user.search"),
    path("user/<int:user_id>/", user_details, name="mylab.admin.user"),
    path("user/<int:user_id>/as_user/", user_details_as_user, name="mylab.admin.user.as_user"),
    path("join/<int:user_id>/accept/", accept_join, name="mylab.admin.accept_join"),
    path("join/<int:user_id>/reject/", reject_join, name="mylab.admin.reject_join"),
    path("emails/send/", send_emails, name="mylab.admin.emails.send"),
    path("emails/", list_emails, name="mylab.admin.emails.list"),
    path("emails/details/<int:email_id>/", email_details, name="mylab.admin.emails.details"),
    path("studip/", import_studip, name="mylab.admin.studip"),
    path("appointment/<int:appointment_id>/", appointment, name="mylab.admin.appointment"),
]
