from django import forms


class CloseExamForm(forms.Form):
    passed = forms.ChoiceField(
        label="Result", required=True, choices=[(None, "---"), (True, "Passed"), (False, "Failed")]
    )
    send_email = forms.BooleanField(label="Send email confirmation", initial=True, required=False)
