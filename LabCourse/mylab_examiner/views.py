from django.contrib.auth.decorators import user_passes_test
from django.db.transaction import atomic
from django.http import Http404
from django.shortcuts import render

from LabCourse.api.models import Exam, ExamUpdate, UpdateModel
from LabCourse.mylab_examiner.forms import CloseExamForm


@user_passes_test(lambda u: u.is_authenticated and (u.is_examiner or u.is_admin))
def examiner_help(request):
    return render(request, "mylab_examiner/help.html")


@user_passes_test(lambda u: u.is_authenticated and (u.is_examiner or u.is_admin))
@atomic
def close_exam(request, exam_id):
    # get exam
    try:
        exam = Exam.objects.get(pk=exam_id)
    except Exam.DoesNotExist:
        raise Http404

    # check whether examiner can close it
    if not request.user.is_user_or_delegate(exam.examiner) and not request.user.is_admin:
        raise Http404

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = CloseExamForm(request.POST, request.FILES)

        # cannot accept already closed exam
        if exam.passed is not None:
            form.add_error(None, "Cannot close already closed exam.")

        # check whether it's valid:
        elif form.is_valid():
            # close it
            exam.passed = form.cleaned_data["passed"]
            exam.save()

            # store update
            ExamUpdate(exam=exam, user=request.user, action=UpdateModel.Action.CLOSED).save()

            # send email to student, examiner and admins
            # if "send_email" not in form.cleaned_data or form.cleaned_data["send_email"] is True:
            #    send_email_exam_closed(exam)

            # render
            return render(
                request,
                "mylab_examiner/close_exam.html",
                {"mylab_page": "Examiner", "exam": exam, "success": True},
            )

    else:
        # create form
        form = CloseExamForm()

    # render
    return render(
        request,
        "mylab_examiner/close_exam.html",
        {"mylab_page": "Examiner", "exam": exam, "form": form},
    )
