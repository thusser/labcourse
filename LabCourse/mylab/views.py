import datetime
from typing import Optional, Dict, Any
from urllib.parse import urljoin

from django.contrib.auth.decorators import user_passes_test
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from LabCourse import settings
from LabCourse.api.models import (
    Experiment,
    Semester,
    Appointment,
    LabUser,
)
from LabCourse.mylab.auth import logged_in_or_basicauth
from LabCourse.mylab.calendar import create_ics


def get_user_home_data(user: Optional[LabUser] = None, semester: Optional[Semester] = None) -> Dict[str, Any]:
    return {
        "labuser": user,
        "semester": semester,
        "admin": user is None,
    }


class MyLabView(TemplateView):
    template_name = "mylab/home.html"

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)

        # get all active experiments
        experiments = Experiment.objects.filter(active=True)

        # get current semester and all dates
        semester = Semester.current()
        dates = semester.semesterdate_set.filter(special=False, date__gte=datetime.date.today())

        # loop all experiments
        overview = []
        for exp in experiments:
            overview.append(
                {
                    "experiment": exp,
                    "appointments": [Appointment.objects.filter(experiment=exp, date=date).exists() for date in dates],
                }
            )

        # get data
        context = {
            "data": overview,
            "dates": dates,
            "mylab_page": None,
        }
        if self.request.user.is_authenticated:
            context.update(**get_user_home_data(self.request.user))
        return context


@user_passes_test(lambda u: u.is_authenticated)
def calendar(request):
    # does user have token?
    if request.user.calendar_token is None:
        request.user.create_calendar_token()

    # render page
    return render(request, "mylab/calendar.html", {"url": urljoin(settings.ROOT_URL, "mylab/calendar/ALC.ics")})


@logged_in_or_basicauth(realm="ALC calendar")
def calendar_ics(request):
    # create ics
    ics = create_ics(request.user)

    # return response
    return HttpResponse(ics, content_type="text/calendar")
