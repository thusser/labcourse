"""LabCourse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, include

from .views import MyLabView, calendar, calendar_ics

urlpatterns = [
    path("", MyLabView.as_view(), name="mylab"),
    path("administrator/", include("LabCourse.mylab_admin.urls")),
    path("examiner/", include("LabCourse.mylab_examiner.urls")),
    path("tutor/", include("LabCourse.mylab_tutor.urls")),
    path("student/", include("LabCourse.mylab_student.urls")),
    path("calendar/", calendar, name="mylab.calendar"),
    path("calendar/ALC.ics", calendar_ics, name="mylab.calendar.ics"),
]
