import datetime
from typing import List
from urllib.parse import urljoin

from django.db.models import Q

from LabCourse import settings
from LabCourse.api.models import Appointment, LabUser, Exam


def _create_event(
    uid: str, location: str, summary: str, desc: str, start: datetime.datetime, end: datetime.datetime
) -> List[str]:
    return [
        "BEGIN:VEVENT",
        f"UID:{uid }",
        f"LOCATION:{location}",
        f"SUMMARY:{summary}",
        f"DESCRIPTION:{desc}",
        "CLASS:PUBLIC",
        f'DTSTART;TZID=Europe/Berlin:{start.strftime("%Y%m%dT%H%M%S")}',
        f'DTEND;TZID=Europe/Berlin:{end.strftime("%Y%m%dT%H%M%S")}',
        f'DTSTAMP;TZID=Europe/Berlin:{datetime.datetime.now().strftime("%Y%m%dT%H%M%S")}',
        "END:VEVENT",
    ]


def _create_appointment_event(appointment: Appointment) -> List[str]:
    """Create single entry in ICS file."""

    # get experiment
    exp = appointment.experiment

    # start/end
    d = appointment.date.date
    start = datetime.datetime.combine(d, datetime.time(9, 00))
    end = datetime.datetime.combine(d, datetime.time(17, 00))

    # names
    students = "\\n".join([f"- {u.full_name} <{u.email}>" for u in appointment.students.all()])
    tutors = "\\n".join([f"- {u.full_name} <{u.email}>" for u in appointment.experiment.tutors.all()])

    # description
    desc = (
        f'ALC lab appointment for experiment "{exp.code} - {exp.name}"\\n\\n'
        f"Student(s):\\n{students}\\n\\nTutor(s):\\n{tutors}"
    )

    # build content
    return _create_event(
        f"ALC_appointment_{appointment.pk}",
        exp.location,
        f"ALC lab: {exp.code}",
        desc,
        start,
        end,
    )


def _create_exam_event(exam: Exam, user: LabUser) -> List[str]:
    """Create single entry in ICS file."""

    # start/end
    start = exam.date
    end = start + datetime.timedelta(hours=1)

    # examiner
    examiner = exam.examiner

    # title depends on whether student or examiner
    title = "ALC exam"
    if exam.examiner == user:
        title += f": {exam.student.full_name}"

    # description
    desc = (
        f"ALC exam\\n\\n"
        f"Student: {exam.student.full_name} <{exam.student.email}>\\n\\n"
        f"Examiner: {exam.examiner.full_name} <{exam.examiner.email}>\\n\\nExperiments:\\n"
    )
    for report in exam.reports.all():
        desc += f"- {report.appointment.experiment.code}"
        if report.report_url is not None:
            desc += f" ({urljoin(settings.ROOT_URL, report.report_url)})"
        desc += "\\n"

    # build content
    a = _create_event(
        f"ALC_exam_{exam.pk}",
        f"{examiner.address}, Room {examiner.room}",
        title,
        desc,
        start,
        end,
    )
    return a


def _format_ics(ics_lines: List[str]) -> str:
    # loop all lines
    result = ""
    for line in ics_lines:
        # fold lines that are longer than 75 characters
        while len(line) > 75:
            result += line[:75] + "\r\n"
            line = " " + line[75:]

        # add remainder
        result += line + "\r\n"

    # finished
    return result


def create_ics(user: LabUser) -> str:
    """Create ICS file for download."""

    # get appointments and exams for user
    appointments = Appointment.objects.filter(Q(students__in=[user]) | Q(experiment__tutors__in=[user]))
    exams = Exam.objects.filter(Q(student__in=[user]) | Q(examiner__in=[user]))

    # create ICS events
    event_lines = []
    for app in appointments:
        event_lines.extend(_create_appointment_event(app))
    for ex in exams:
        event_lines.extend(_create_exam_event(ex, user))

    # format lines
    events = _format_ics(event_lines)

    # build content
    return f"""\
BEGIN:VCALENDAR
VERSION:2.0
PRODID:advanced-lab-course.physik.uni-goettingen.de
METHOD:PUBLISH
{events}
END:VCALENDAR"""
