import logging
from django.core.mail import EmailMessage

from LabCourse import settings
from LabCourse.api.models import Appointment, LabUser, JoinRequest, Exam, Report, AppointmentRequest


def _get_admin_emails():
    emails = [admin.email for admin in LabUser.objects.filter(is_admin=True)]
    return emails if len(emails) > 0 else None


def _send_mail(subject: str, message: str, to: str, bcc_admins: bool = False):
    bcc = _get_admin_emails() if bcc_admins else None
    EmailMessage(subject, message, settings.EMAIL_FROM, to=[to], bcc=bcc).send()


def send_email_appointment(recipient: LabUser, appointment: Appointment, subject: str, intro: str, bcc_admins: bool):
    # get list of students and tutors
    students = "\n".join([f"- {u.full_name} <{u.email}>" for u in appointment.students.all()])
    tutors = "\n".join([f"- {u.full_name} <{u.email}>" for u in appointment.experiment.tutors.all()])

    # build body
    body = f"""\
Dear {recipient.full_name},

{intro}

Experiment:
{appointment.experiment.code} - {appointment.experiment.name}

Date:
{appointment.date.date.strftime("%A, %-d %B %Y")}

Student(s):
{students}

Tutor(s):
{tutors}

Regards,
Advanced Lab Course    
"""

    # send it
    logging.info(f"Sending email '{subject}' to {recipient.email}.")
    _send_mail(subject, body, recipient.email, bcc_admins=bcc_admins)


def send_email_new_appointment(appointment: Appointment):
    subject = "[ALC] New lab appointment"
    intro = "an ALC lab appointment has just been created with the following details."
    bcc_admins = settings.LABCOURSE["ADMIN"]["SEND_EMAIL_NEW_APPOINTMENT"]
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_NEW_APPOINTMENT"]:
        for student in appointment.students.all():
            send_email_appointment(student, appointment, subject, intro, bcc_admins=bcc_admins)
    if settings.LABCOURSE["TUTOR"]["SEND_EMAIL_NEW_APPOINTMENT"]:
        for tutor in appointment.experiment.tutors.all():
            send_email_appointment(tutor, appointment, subject, intro, bcc_admins=bcc_admins)


def send_email_appointment_removed(appointment: Appointment):
    subject = "[ALC] Lab appointment canceled"
    intro = "an ALC lab appointment has just been canceled."
    bcc_admins = settings.LABCOURSE["ADMIN"]["SEND_EMAIL_APPOINTMENT_CANCELED"]
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_APPOINTMENT_CANCELED"]:
        for student in appointment.students.all():
            send_email_appointment(student, appointment, subject, intro, bcc_admins=bcc_admins)
    if settings.LABCOURSE["TUTOR"]["SEND_EMAIL_APPOINTMENT_CANCELED"]:
        for tutor in appointment.experiment.tutors.all():
            send_email_appointment(tutor, appointment, subject, intro, bcc_admins=bcc_admins)


def send_email_appointment_changed(appointment: Appointment):
    subject = "[ALC] Lab appointment changed"
    intro = "an ALC lab appointment has just been changed."
    bcc_admins = settings.LABCOURSE["ADMIN"]["SEND_EMAIL_APPOINTMENT_CHANGED"]
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_APPOINTMENT_CHANGED"]:
        for student in appointment.students.all():
            send_email_appointment(student, appointment, subject, intro, bcc_admins=bcc_admins)
    if settings.LABCOURSE["TUTOR"]["SEND_EMAIL_APPOINTMENT_CHANGED"]:
        for tutor in appointment.experiment.tutors.all():
            send_email_appointment(tutor, appointment, subject, intro, bcc_admins=bcc_admins)


def send_email_appointment_request(
    recipient: LabUser, request: AppointmentRequest, subject: str, intro: str, bcc_admins: bool
):
    # build body
    body = f"""\
Dear {recipient.full_name},

{intro}

Experiment:
{request.experiment.code} - {request.experiment.name}

Date:
{request.date.date.strftime("%A, %-d %B %Y")}

Student:
{request.student.full_name} <{request.student.email}>

Lab partner(s):
{request.partners_list()}

Comment:
{request.comment}

Regards,
Advanced Lab Course    
"""

    # send it
    logging.info(f"Sending email '{subject}' to {recipient.email}.")
    _send_mail(subject, body, to=recipient.email, bcc_admins=bcc_admins)


def send_email_new_appointment_request(request: AppointmentRequest):
    subject = "[ALC] New appointment request"
    intro = "a request for an ALC lab appointment has just been created with the following details."
    bcc_admins = settings.LABCOURSE["ADMIN"]["SEND_EMAIL_NEW_APPOINTMENT_REQUEST"]
    if settings.LABCOURSE["TUTOR"]["SEND_EMAIL_NEW_APPOINTMENT_REQUEST"]:
        for tutor in request.experiment.tutors.all():
            send_email_appointment_request(tutor, request, subject, intro, bcc_admins=bcc_admins)


def send_email_join(request: JoinRequest):
    # if not set, do nothing
    if not settings.LABCOURSE["ADMIN"]["SEND_EMAIL_JOIN_REQUEST"]:
        return

    # loop all admins
    for admin in LabUser.objects.filter(is_admin=True):
        # define body of email
        body = f"""\
Dear {admin.full_name},

a new user {request.user.full_name} requests to join the lab course. Please visit the homepage
to handle the request.

Regards,
Advanced Lab Course    
"""

        # send it
        logging.info(f"Sending email '[ALC] New join request' to {admin.email}.")
        _send_mail("[ALC] New join request", body, admin.email)


def send_email_join_accepted(user: LabUser):
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_JOIN_REQUEST"]:
        body = f"""\
Dear {user.full_name},

your request to join the advanced lab course this semester has been granted.
You can now log in on the ALC homepage and use the My Lab page to organize your labs.

Regards,
Advanced Lab Course"""
        _send_mail("[ALC] Join request granted", body, user.email)


def send_email_join_rejected(user: LabUser):
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_JOIN_REQUEST"]:
        body = f"""\
Dear {user.full_name},

your request to join the advanced lab course this semester has been rejected.
Please contact the organizer of the ALC to get more information.

Regards,
Advanced Lab Course"""
        _send_mail("[ALC] Join request rejected", body, user.email)


def send_email_exam(recipient: LabUser, exam: Exam, subject: str, intro: str, bcc_admins: bool):
    # get experiments
    experiments = ""
    for rep in exam.reports.all():
        experiments += f"- {rep.appointment.experiment.code} - {rep.appointment.experiment.name}"
        if rep.report_url:
            experiments += f"\n  Report: {rep.report_url}"
        experiments += "\n"

    # build body
    body = f"""\
Dear {recipient.full_name},

{intro}

Date:
{exam.date.strftime("%A, %-d %B %Y")}

Student:
{exam.student.full_name} <{exam.student.email}>

Examiner:
{exam.examiner.full_name} <{exam.examiner.email}>

Experiments:
{experiments}

Regards,
Advanced Lab Course    
"""

    # send it
    logging.info(f"Sending email '{subject}' to {recipient.email}.")
    _send_mail(subject, body, to=recipient.email, bcc_admins=bcc_admins)


def send_email_new_exam(exam: Exam):
    subject = "[ALC] New exam"
    intro = "an ALC exam has just been created with the following details."
    bcc_admins = settings.LABCOURSE["ADMIN"]["SEND_EMAIL_NEW_EXAM"]
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_NEW_EXAM"]:
        send_email_exam(exam.student, exam, subject, intro, bcc_admins=bcc_admins)
    if settings.LABCOURSE["EXAMINER"]["SEND_EMAIL_NEW_EXAM"]:
        send_email_exam(exam.examiner, exam, subject, intro, bcc_admins=bcc_admins)


"""
def send_email_exam_closed(exam: Exam):
    result = "passed" if exam.passed else "failed"
    subject = f"[ALC] Exam {result}"
    intro = f"an ALC exam has just been closed with the following details.\n\nResult: {result}"
    bcc_admins = settings.LABCOURSE["ADMIN"]["SEND_EMAIL_EXAM_CLOSED"]
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_EXAM_CLOSED"]:
        send_email_exam(exam.student, exam, subject, intro, bcc_admins=bcc_admins)
    if settings.LABCOURSE["EXAMINER"]["SEND_EMAIL_EXAM_CLOSED"]:
        send_email_exam(exam.examiner, exam, subject, intro, bcc_admins=bcc_admins)
"""


def send_email_report(recipient: LabUser, report: Report, subject: str, intro: str, bcc_admins: bool):
    # tutor
    if report.tutor is None:
        tutors = "\n".join([f"{u.full_name} <{u.email}>" for u in report.appointment.experiment.tutors.all()])
    else:
        tutors = f"{report.tutor.full_name} <{report.tutor.email}>"

    # build body
    body = f"""\
Dear {recipient.full_name},

{intro}

Student:
{report.student.full_name} <{report.student.email}>

Tutor(s):
{tutors}

Experiment:
{report.appointment.experiment.code} - {report.appointment.experiment.name}

Regards,
Advanced Lab Course    
"""

    # send it
    logging.info(f"Sending email '{subject}' to {recipient.email}.")
    _send_mail(subject, body, to=recipient.email, bcc_admins=bcc_admins)


def send_email_report_accepted(report: Report):
    subject = "[ALC] Report accepted"
    intro = "an ALC report has just been accepted with the following details."
    bcc_admins = settings.LABCOURSE["ADMIN"]["SEND_EMAIL_REPORT_ACCEPTED"]
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_REPORT_ACCEPTED"]:
        send_email_report(
            report.student, report, subject, intro + " You may now upload the PDF.", bcc_admins=bcc_admins
        )
    if settings.LABCOURSE["TUTOR"]["SEND_EMAIL_REPORT_ACCEPTED"]:
        send_email_report(report.tutor, report, subject, intro, bcc_admins=bcc_admins)


def send_email_report_rejected(report: Report):
    subject = "[ALC] Lab failed"
    intro = "an ALC report has just been rejected, so unfortunately the corresponding lab has been failed."
    bcc_admins = settings.LABCOURSE["ADMIN"]["SEND_EMAIL_REPORT_ACCEPTED"]
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_REPORT_ACCEPTED"]:
        send_email_report(report.student, report, subject, intro, bcc_admins=bcc_admins)
    if settings.LABCOURSE["TUTOR"]["SEND_EMAIL_REPORT_ACCEPTED"]:
        send_email_report(report.tutor, report, subject, intro, bcc_admins=bcc_admins)


def _send_email_report_uploaded(recipient: LabUser, report: Report, bcc_admins: bool):
    # tutor
    if report.tutor is None:
        tutors = "\n".join([f"{u.full_name} <{u.email}>" for u in report.appointment.experiment.tutors.all()])
    else:
        tutors = f"{report.tutor.full_name} <{report.tutor.email}>"

    # build body
    body = f"""\
Dear {recipient.full_name},

an ALC report has just been uploaded:
{report.report_url}

Student:
{report.student.full_name} <{report.student.email}>

Tutor(s):
{tutors}>

Experiment:
{report.appointment.experiment.code} - {report.appointment.experiment.name}

Regards,
Advanced Lab Course    
"""

    # send it
    logging.info(f"Sending email '[ALC] Report uploaded' to {recipient.email}.")
    _send_mail("[ALC] Report uploaded", body, to=recipient.email, bcc_admins=bcc_admins)


def send_email_report_uploaded(report: Report):
    bcc_admins = settings.LABCOURSE["ADMIN"]["SEND_EMAIL_REPORT_UPLOADED"]
    if settings.LABCOURSE["STUDENT"]["SEND_EMAIL_REPORT_UPLOADED"]:
        _send_email_report_uploaded(report.student, report, bcc_admins=bcc_admins)
    if settings.LABCOURSE["TUTOR"]["SEND_EMAIL_REPORT_UPLOADED"]:
        if report.tutor is not None:
            _send_email_report_uploaded(report.tutor, report, bcc_admins=bcc_admins)
        else:
            for tutor in report.appointment.experiment.tutors.all():
                _send_email_report_uploaded(tutor, report, bcc_admins=bcc_admins)
