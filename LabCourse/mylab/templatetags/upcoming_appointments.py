import datetime
from collections import defaultdict

from django import template
from django.template.loader import render_to_string

from LabCourse.api.models import LabUser, Semester

register = template.Library()


@register.simple_tag
def upcoming_appointments(user: LabUser, semester: Semester, admin: bool):
    from LabCourse.api.models import Appointment

    # get all upcoming appointments
    tmp = Appointment.user_appointments(user, semester).filter(date__date__gte=datetime.date.today())
    schedule = defaultdict(lambda: [])
    for a in tmp:
        if a not in schedule[a.date.date]:
            schedule[a.date.date].append(a)

    # render it
    return render_to_string(
        "mylab/upcoming_appointments.html", {"schedule": dict(schedule), "labuser": user, "admin": admin}
    )
