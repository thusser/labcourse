from collections import defaultdict

from django import template
from django.template.loader import render_to_string

from LabCourse import settings
from LabCourse.api.models import LabUser, Semester

register = template.Library()


@register.simple_tag
def appointment_requests(user: LabUser, semester: Semester, admin: bool):
    from LabCourse.api.models import AppointmentRequest

    # get all requests the user is related to
    tmp = AppointmentRequest.user_requests(user, semester)
    requests = defaultdict(lambda: [])
    for a in tmp:
        key = (a.date, a.experiment)
        if a not in requests[key]:
            requests[key].append(a)

    # render it
    return render_to_string(
        "mylab/appointment_requests.html",
        {"requests": dict(requests), "labuser": user, "admin": admin, "LABCOURSE": settings.LABCOURSE},
    )
