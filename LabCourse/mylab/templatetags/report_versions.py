from django import template
from django.template.loader import render_to_string

register = template.Library()


@register.simple_tag
def report_versions(report):
    # render it
    return render_to_string("mylab/report_versions.html", {"report": report})
