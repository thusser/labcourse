from django import template
from django.template.loader import render_to_string

from LabCourse.api.models import LabUser, Semester

register = template.Library()


@register.simple_tag
def pending_reports(user: LabUser, semester: Semester, admin: bool):
    from LabCourse.api.models import Report

    # pending reports
    reports = Report.pending(user, semester).order_by(
        "appointment__date__date",
    )

    # render it
    return render_to_string("mylab/pending_reports.html", {"reports": reports, "labuser": user, "admin": admin})
