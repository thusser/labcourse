import pandas as pd
from django import template
from django.db.models import Count
from django.template.loader import render_to_string

from LabCourse.api.models import Semester, SemesterDate, Report

register = template.Library()


@register.simple_tag
def student_summary(user, admin):
    from LabCourse.api.models import Exam

    # get data
    summary = {}
    if user is not None:
        summary["appointments"] = []
        for app in user.appointments.all():
            report = app.report_set.filter(student=user).first()
            exam = None if report is None else report.exams.last()
            summary["appointments"].append({"appointment": app, "report": report, "exam": exam})
        summary["exams"] = Exam.objects.filter(student=user).all()

    # render it
    return render_to_string("mylab/student_summary.html", {"summary": summary, "admin": admin})


@register.simple_tag
def tutor_summary(user, admin):
    from LabCourse.api.models import Appointment

    # get data
    appointments = []
    exps = []
    semesters = []
    num_reports = 0
    credits = 0
    if user is not None:
        # list of appointments
        appointments = Appointment.tutor_appointments(user).all()

        # experiments ever tutored
        exps = list(set([a.experiment for a in appointments]))

        # number of semesters, this seems way easier in pandas...
        a = pd.DataFrame(
            Appointment.objects.filter(report__tutor=user).values("date__semester__code", "experiment_id")
        ).drop_duplicates()
        if "experiment_id" in a:
            for exp in exps:
                a2 = a[a["experiment_id"] == exp.code]
                sems = a2["date__semester__code"].unique()
                semesters.append({"experiment": exp.code, "semesters": ", ".join(sems), "credits": len(sems)})
                credits += len(sems)

        # number of reports
        num_reports = Report.objects.filter(tutor=user, passed__isnull=False).count()
        credits += num_reports // 4

    # count total students for experiments
    experiments = {exp: sum([a.students.count() for a in exp.appointment_set.all()]) for exp in exps}

    # render it
    return render_to_string(
        "mylab/tutor_summary.html",
        {
            "appointments": appointments,
            "experiments": experiments,
            "semesters": semesters,
            "num_reports": num_reports,
            "report_credits": num_reports // 4,
            "credits": credits,
            "admin": admin,
        },
    )


@register.simple_tag
def examiner_summary(user, admin):
    from LabCourse.api.models import Exam

    # get data
    summary = []
    if user is not None:
        summary = Exam.objects.filter(examiner=user).all()

    # render it
    return render_to_string("mylab/tutor_summary.html", {"summary": summary, "admin": admin})
