import datetime
from collections import defaultdict

from django import template
from django.template.loader import render_to_string

from LabCourse.api.models import LabUser, Semester

register = template.Library()


@register.simple_tag
def previous_appointments(user: LabUser, semester: Semester, admin: bool):
    from LabCourse.api.models import Appointment

    # and all past open appointments
    tmp = Appointment.user_appointments(user, semester).filter(date__date__lte=datetime.date.today(), is_open=True)
    finished = defaultdict(lambda: [])
    for a in tmp:
        if a not in finished[a.date.date]:
            finished[a.date.date].append(a)

    # render it
    return render_to_string(
        "mylab/previous_appointments.html", {"finished": dict(finished), "labuser": user, "admin": admin}
    )
