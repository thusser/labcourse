import datetime
from collections import defaultdict

from django import template
from django.template.loader import render_to_string

from LabCourse.api.models import LabUser, Semester

register = template.Library()


@register.simple_tag
def upcoming_exams(user: LabUser, semester: Semester, admin: bool):
    from LabCourse.api.models import Exam

    # exams take 30 minutes, so find all exams that will start in the future or have started up to 30 minutes
    # in the past
    end = datetime.datetime.now() - datetime.timedelta(minutes=30)

    # get all upcoming exams
    exams = Exam.user_exams(user, semester).filter(date__gte=end)

    # render it
    return render_to_string("mylab/upcoming_exams.html", {"exams": exams, "labuser": user, "admin": admin})
