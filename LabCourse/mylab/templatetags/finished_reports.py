from collections import defaultdict

from django import template
from django.template.loader import render_to_string

from LabCourse import settings
from LabCourse.api.models import LabUser, Semester

register = template.Library()


@register.simple_tag
def finished_reports(user: LabUser, semester: Semester, admin: bool):
    from LabCourse.api.models import Report

    # finished reports
    tmp = Report.finished(user, semester).order_by("student__last_name", "student__first_name")
    reports_pending = 0
    if user is None:
        reports = defaultdict(lambda: [])
        for r in tmp:
            if r not in reports:
                reports[r.student].append(r)
        reports = dict(reports)
    else:
        reports = tmp
        reports_pending = reports.filter(exams__isnull=True).count()

    # render it
    return render_to_string(
        "mylab/finished_reports_admin.html" if user is None else "mylab/finished_reports.html",
        {
            "reports": reports,
            "reports_pending": reports_pending,
            "labuser": user,
            "admin": admin,
            # TODO: need to find out, why we have to pass LABCOURSE here. Don't need to do that in all other templates,
            #  also not in appointment_requests.html.
            "LABCOURSE": settings.LABCOURSE,
        },
    )
