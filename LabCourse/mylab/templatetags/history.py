from django import template
from django.template.loader import render_to_string

register = template.Library()


@register.simple_tag
def history(updates, admin: bool = True):
    return render_to_string("mylab/history.html", {"updates": updates, "admin": admin})
