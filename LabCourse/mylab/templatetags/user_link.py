from django import template
from django.template.loader import render_to_string

from LabCourse.api.models import Experiment, LabUser

register = template.Library()


@register.simple_tag
def user_link(user: LabUser, admin: bool = False, fullname: bool = False):
    return render_to_string("mylab/user_link.html", {"user": user, "admin": admin, "fullname": fullname})


@register.simple_tag
def user_link_list(users, admin: bool = False, newline: bool = True, fullname: bool = False):
    return render_to_string(
        "mylab/user_link_list.html", {"users": users, "admin": admin, "newline": newline, "fullname": fullname}
    )


@register.simple_tag
def tutor_link(user: LabUser, admin: bool = False, fullname: bool = False):
    return render_to_string("mylab/tutor_link.html", {"user": user, "admin": admin, "fullname": fullname})


@register.simple_tag
def tutor_link_list(users, admin: bool = False, newline: bool = True, fullname: bool = False):
    return render_to_string(
        "mylab/tutor_link_list.html", {"users": users, "admin": admin, "newline": newline, "fullname": fullname}
    )


@register.simple_tag
def examiner_link(examiner: LabUser, admin: bool = False, fullname: bool = False):
    return render_to_string("mylab/examiner_link.html", {"examiner": examiner, "admin": admin, "fullname": fullname})


@register.simple_tag
def examiner_link_list(user: LabUser, admin: bool = False, newline: bool = True, fullname: bool = False):
    return render_to_string(
        "mylab/examiner_link_list.html", {"user": user, "admin": admin, "newline": newline, "fullname": fullname}
    )


@register.simple_tag
def experiment_link(experiment: Experiment, full: bool = False):
    return render_to_string("mylab/experiment_link.html", {"experiment": experiment, "full": full})
