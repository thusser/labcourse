from django import template
from django.template.loader import render_to_string

register = template.Library()


@register.simple_tag
def join_requests():
    from LabCourse.api.models import JoinRequest

    # join requests
    requests = JoinRequest.objects.all()

    # render it
    return render_to_string("mylab/join_requests.html", {"join_requests": requests})
