import datetime
import uuid

from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import IntegrityError
from django.db.transaction import atomic
from django.http import Http404
from django.shortcuts import render

from LabCourse.api.models import (
    Semester,
    Report,
    Experiment,
    SemesterDate,
    Appointment,
    LabUser,
    AppointmentRequest,
    AppointmentUpdate,
    UpdateModel,
    ReportUpdate,
    LabEvaluationRequest,
)
from LabCourse.frontend.email import send_email_evaluation_request
from LabCourse.mylab.email import (
    send_email_report_accepted,
    send_email_new_appointment,
    send_email_appointment_changed,
    send_email_appointment_removed,
    send_email_report_rejected,
)
from LabCourse.mylab_tutor.forms import (
    AcceptReportForm,
    AddAppointmentForm,
    RemoveAppointmentForm,
    CloseAppointmentForm,
    ProcessAppointmentRequestForm,
    RemoveAppointmentRequestForm,
    UploadInstructionsForm,
)
from LabCourse.settings import LABCOURSE


@user_passes_test(lambda u: u.is_authenticated and (u.is_tutor or u.is_admin))
def students(request):
    # get current semester
    semester = Semester.current()

    # render
    return render(request, "mylab_tutor/students.html", {"mylab_page": "Admin", "semester": semester})


@user_passes_test(lambda u: u.is_authenticated and (u.is_tutor or u.is_admin))
def tutor_help(request):
    return render(request, "mylab_tutor/help.html")


@user_passes_test(lambda u: u.is_authenticated and (u.is_tutor or u.is_admin))
@atomic
def process_appointment_request(request, experiment_code: str, date_str: str):
    # get experiment and date
    try:
        experiment = Experiment.objects.get(code=experiment_code)
        date = SemesterDate.objects.get(date=date_str)
    except (Experiment.DoesNotExist, SemesterDate.DoesNotExist):
        raise Http404

    # semester dates
    semester = Semester.current()
    dates = semester.semesterdate_set.all()

    # does appointment already exist?
    exists = Appointment.objects.filter(experiment=experiment, date=date).exists()

    # get all requests
    requests = AppointmentRequest.objects.filter(experiment=experiment, date=date)

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = ProcessAppointmentRequestForm(request.POST, requests=requests, dates=dates, date=date)

        # check whether it's valid:
        if form.is_valid():
            # get date
            selected_date = SemesterDate.objects.get(pk=int(form.cleaned_data["date"]))

            # if we got at least one student, we create an appointment
            if "students" in form.cleaned_data and len(form.cleaned_data["students"]) > 0:
                # create Appointment
                appointment = Appointment()
                appointment.experiment = experiment
                appointment.date = selected_date
                appointment.save()

                # add students
                for student_id in form.cleaned_data["students"]:
                    # get and add student
                    student = LabUser.objects.get(pk=int(student_id))
                    appointment.students.add(student)

                    # remove request for student
                    requests.filter(student=student).delete()

                # close appointment, if in the past
                appointment.close_if_past()

                # store update
                AppointmentUpdate(appointment=appointment, user=request.user, action=UpdateModel.Action.ADDED).save()

                # send email to students, tutors and admins
                if "send_email" not in form.cleaned_data or form.cleaned_data["send_email"] is True:
                    send_email_new_appointment(appointment)

            # finished
            return render(
                request,
                "mylab_tutor/process_appointment_request.html",
                {"success": True, "appointment": appointment},
            )

    else:
        # create form
        form = ProcessAppointmentRequestForm(requests=requests, dates=dates, date=date)

    # render
    return render(
        request,
        "mylab_tutor/process_appointment_request.html",
        {
            "mylab_page": "Tutor",
            "experiment": experiment,
            "date": date,
            "requests": requests,
            "form": form,
            "exists": exists,
        },
    )


def show_remove_appointment_request(request, req):
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = RemoveAppointmentRequestForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            req.delete()
            return render(
                request,
                "mylab_tutor/remove_appointment_request.html",
                {"mylab_page": "Tutor", "request": req, "success": True},
            )

    else:
        # create form
        form = RemoveAppointmentRequestForm()

    # render
    return render(
        request,
        "mylab_tutor/remove_appointment_request.html",
        {"mylab_page": "Tutor", "request": req, "form": form},
    )


@user_passes_test(lambda u: u.is_authenticated and (u.is_tutor or u.is_admin or u.is_student))
@atomic
def remove_appointment_request(request, request_id):
    # allowed?
    if request.user.is_student and not LABCOURSE["STUDENT"]["CAN_CANCEL_APPOINTMENT_REQUEST"]:
        raise Http404()

    # get request
    try:
        req = AppointmentRequest.objects.get(pk=request_id)
    except AppointmentRequest.DoesNotExist:
        raise Http404

    # check whether tutor can remove it
    if req.experiment.is_tutor(request.user) or req.student == request.user or request.user.is_admin:
        return show_remove_appointment_request(request, req)
    else:
        raise Http404


@user_passes_test(lambda u: u.is_authenticated and (u.is_tutor or u.is_admin))
@atomic
def add_appointment(request):
    # get list of students for current semester and dates
    semester = Semester.current()
    students = semester.students.all()

    # get experiments for user
    experiments = (
        Experiment.objects.filter()
        if request.user.is_admin
        else Experiment.objects.filter(tutors__in=[request.user], active=True)
    )

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddAppointmentForm(request.POST, experiments=experiments)

        # check whether it's valid:
        if form.is_valid():
            try:
                # get experiment and date
                c = form.cleaned_data
                experiment = Experiment.objects.get(code=c["experiment"].upper())
                date = SemesterDate.objects.get(pk=int(c["date"]))

                # add appointment
                appointment = Appointment(experiment=experiment, date=date)
                appointment.save()

                # add students
                for i in [1, 2, 3]:
                    k = f"student{i}"
                    if c[k] is not None:
                        student = LabUser.objects.get(pk=int(c[k]))
                        appointment.students.add(student)

                # close appointment, if in the past
                appointment.close_if_past()

                # store update
                AppointmentUpdate(appointment=appointment, user=request.user, action=UpdateModel.Action.ADDED).save()

                # send email to students, tutors and admins
                if "send_email" not in form.cleaned_data or form.cleaned_data["send_email"] is True:
                    send_email_new_appointment(appointment)

                # render
                return render(
                    request,
                    "mylab_tutor/add_appointment.html",
                    {"success": True, "appointment": appointment},
                )

            except ObjectDoesNotExist:
                # show the form again
                pass

            except IntegrityError:
                # add non-field-error
                form.add_error(None, "The experiment is already booked for that date.")

    else:
        # create form
        form = AddAppointmentForm(experiments=experiments)

    # render
    return render(
        request,
        "mylab_tutor/add_appointment.html",
        {"mylab_page": "Tutor", "form": form, "edit": False},
    )


@user_passes_test(lambda u: u.is_authenticated and (u.is_tutor or u.is_admin))
@atomic
def edit_appointment(request, appointment_id: int):
    # get appointment
    try:
        appointment = Appointment.objects.get(pk=appointment_id)
    except Appointment.DoesNotExist:
        raise Http404

    # get experiments for user
    experiments = (
        Experiment.objects.filter(active=True)
        if request.user.is_admin
        else Experiment.objects.filter(tutors__in=[request.user], active=True)
    )

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddAppointmentForm(request.POST, experiments=experiments)

        # check whether it's valid:
        if form.is_valid():
            try:
                # get experiment and date
                c = form.cleaned_data
                experiment = Experiment.objects.get(code=c["experiment"].upper())
                date = SemesterDate.objects.get(pk=int(c["date"]))

                # edit appointment
                appointment.experiment = experiment
                appointment.date = date
                appointment.save()

                # add students
                appointment.students.clear()
                for i in [1, 2, 3]:
                    k = f"student{i}"
                    if c[k] is not None:
                        student = LabUser.objects.get(pk=int(c[k]))
                        appointment.students.add(student)

                # store update
                AppointmentUpdate(appointment=appointment, user=request.user, action=UpdateModel.Action.CHANGED).save()

                # send email to students, tutors and admins
                if "send_email" not in form.cleaned_data or form.cleaned_data["send_email"] is True:
                    send_email_appointment_changed(appointment)

                # render
                return render(
                    request,
                    "mylab_tutor/add_appointment.html",
                    {"mylab_page": "Tutor", "success": True, "appointment": appointment},
                )

            except ObjectDoesNotExist:
                # show the form again
                pass

            except IntegrityError:
                # add non-field-error
                form.add_error(None, "The experiment is already booked for that date.")

    else:
        # initial values
        initial = {
            "experiment": appointment.experiment.code.lower(),
            "date": appointment.date_id,
        }

        # add students
        for i, s in enumerate(appointment.students.all(), 1):
            initial[f"student{i}"] = s.pk

        # create form
        form = AddAppointmentForm(experiments=experiments, initial=initial)

    # render
    return render(
        request,
        "mylab_tutor/add_appointment.html",
        {"mylab_page": "Tutor", "form": form, "edit": True, "appointment": appointment},
    )


@user_passes_test(lambda u: u.is_authenticated and (u.is_tutor or u.is_admin))
@atomic
def remove_appointment(request, appointment_id):
    # get appointment
    try:
        appointment = Appointment.objects.get(pk=appointment_id)
    except Appointment.DoesNotExist:
        raise Http404

    # check whether tutor can remove it
    if appointment.experiment.is_tutor(request.user) or request.user.is_admin:
        return show_remove_appointment(request, appointment)
    else:
        raise Http404


def show_remove_appointment(request, appointment):
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = RemoveAppointmentForm(request.POST)

        # check, whether appointment can be removed
        if appointment.report_set.count() > 0:
            form.add_error(None, "Cannot remove appointments that already have reports attached.")

        # check whether it's valid:
        elif form.is_valid():
            # send email to students, tutors and admins, only if appointment is in the future
            if appointment.date.date >= datetime.date.today():
                if "send_email" not in form.cleaned_data or form.cleaned_data["send_email"] is True:
                    send_email_appointment_removed(appointment)

            # delete appointment
            appointment.delete()

            # show page
            return render(
                request,
                "mylab_tutor/remove_appointment.html",
                {"mylab_page": "Tutor", "appointment": appointment, "success": True},
            )

    else:
        # create form
        form = RemoveAppointmentForm()

    # render
    return render(
        request,
        "mylab_tutor/remove_appointment.html",
        {"mylab_page": "Tutor", "appointment": appointment, "form": form},
    )


@user_passes_test(lambda u: u.is_authenticated and (u.is_tutor or u.is_admin))
@atomic
def close_appointment(request, appointment_id):
    # get appointment
    try:
        appointment = Appointment.objects.get(pk=appointment_id)
    except Appointment.DoesNotExist:
        raise Http404

    # check whether tutor can close it
    if not appointment.experiment.is_tutor(request.user) and not request.user.is_admin:
        raise Http404

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form and validate it
        form = CloseAppointmentForm(request.POST, appointment=appointment)
        if form.is_valid():
            # get students
            students = [LabUser.objects.get(pk=student_id) for student_id in form.cleaned_data["students"]]

            # close appointment
            reports = appointment.close_appointment(students)

            # render page
            return render(
                request,
                "mylab_tutor/close_appointment.html",
                {"appointment": appointment, "success": True, "reports": reports},
            )

    else:
        # create form
        form = CloseAppointmentForm(appointment=appointment)

    # render
    return render(
        request,
        "mylab_tutor/close_appointment.html",
        {"mylab_page": "Tutor", "appointment": appointment, "form": form},
    )


@user_passes_test(lambda u: u.is_authenticated and (u.is_tutor or u.is_admin))
@atomic
def accept_report(request, report_id):
    # get report
    try:
        report = Report.objects.get(pk=report_id)
    except Report.DoesNotExist:
        raise Http404

    # check whether tutor can remove it
    if not report.appointment.experiment.is_tutor(request.user) and not request.user.is_admin:
        raise Http404

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AcceptReportForm(request.POST, request.FILES, appointment=report.appointment, user=request.user)

        # cannot accept already accepted reports
        if report.date is not None:
            form.add_error(None, "Cannot accept already accepted reports.")

        # check whether it's valid:
        elif form.is_valid():
            # get tutor
            tutor = LabUser.objects.get(pk=form.cleaned_data["tutor"])

            # accept it
            report.date = datetime.datetime.now()
            report.passed = form.cleaned_data["passed"]
            report.tutor = tutor
            report.save()

            # store update
            ReportUpdate(report=report, user=request.user, action=UpdateModel.Action.CLOSED).save()

            # send email to student, tutor and admins
            if "send_email" not in form.cleaned_data or form.cleaned_data["send_email"] is True:
                if report.passed:
                    send_email_report_accepted(report)
                else:
                    send_email_report_rejected(report)

                # eval request
                eval_request = LabEvaluationRequest()
                eval_request.experiment = report.appointment.experiment
                eval_request.semester = report.appointment.date.semester
                eval_request.uuid = str(uuid.uuid4())
                eval_request.save()
                send_email_evaluation_request(eval_request, report.student)

            # render
            return render(
                request,
                "mylab_tutor/accept_report.html",
                {"mylab_page": "Tutor", "report": report, "success": True},
            )

    else:
        # create form
        form = AcceptReportForm(appointment=report.appointment, user=request.user)

    # render
    return render(
        request,
        "mylab_tutor/accept_report.html",
        {"mylab_page": "Tutor", "report": report, "form": form},
    )


@user_passes_test(lambda u: u.is_authenticated and (u.is_admin or u.is_tutor))
@atomic
def update_instructions(request):
    # get experiments
    experiments = Experiment.objects
    if request.user.is_admin:
        experiments = Experiment.objects.all()
    else:
        # for all non-admins limit access to labs, in which user is tutor
        experiments = Experiment.objects.filter(tutors__in=[request.user])

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = UploadInstructionsForm(request.POST, request.FILES, experiments=experiments)

        # check whether it's valid:
        if form.is_valid():
            # get experiment
            experiment = Experiment.objects.get(pk=form.cleaned_data["experiment"])

            # save instructions
            experiment.add_instructions(request.FILES["instructions"], request.user)

            # send email to student and admins
            # send_email_report_uploaded(report)

            # render
            return render(
                request,
                "mylab_tutor/update_instructions.html",
                {"mylab_page": "Tutor", "success": True},
            )

    else:
        # create form
        form = UploadInstructionsForm(experiments=experiments)

    # render
    return render(
        request,
        "mylab_tutor/update_instructions.html",
        {"mylab_page": "Tutor", "form": form},
    )


@user_passes_test(lambda u: u.is_authenticated and (u.is_admin or u.is_tutor))
def experiment(request, category: str, experiment: str):
    # get experiment
    exp = Experiment.objects.get(code=category.upper() + "." + experiment.upper())

    # is user tutor?
    if not exp.is_tutor(request.user) and not request.user.is_admin:
        raise PermissionDenied()

    # get appointments
    appointments = Appointment.objects.filter(experiment=exp).all()

    # render it
    return render(
        request,
        "mylab_tutor/experiment.html",
        {"experiment": exp, "appointments": appointments, "admin": request.user.is_admin},
    )
