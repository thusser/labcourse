"""LabCourse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from .views import (
    students,
    tutor_help,
    accept_report,
    add_appointment,
    edit_appointment,
    remove_appointment,
    close_appointment,
    remove_appointment_request,
    process_appointment_request,
    update_instructions,
    experiment,
)

urlpatterns = [
    path("students/", students, name="mylab.tutor.students"),
    path("help/", tutor_help, name="mylab.tutor.help"),
    path("request/<int:request_id>/remove", remove_appointment_request, name="mylab.tutor.remove_appointment_request"),
    path(
        "request/<str:experiment_code>/<str:date_str>",
        process_appointment_request,
        name="mylab.tutor.process_appointment_request",
    ),
    path("appointment/", add_appointment, name="mylab.tutor.add_appointment"),
    path("appointment/<int:appointment_id>/edit", edit_appointment, name="mylab.tutor.edit_appointment"),
    path("appointment/<int:appointment_id>/remove", remove_appointment, name="mylab.tutor.remove_appointment"),
    path("appointment/<int:appointment_id>/close", close_appointment, name="mylab.tutor.close_appointment"),
    path("report/<int:report_id>/process", accept_report, name="mylab.tutor.accept_report"),
    path("instructions/upload", update_instructions, name="mylab.tutor.instructions.update"),
    path("experiments/<str:category>/<str:experiment>/", experiment, name="mylab.tutor.experiment"),
]
