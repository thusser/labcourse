from typing import List, Tuple, Optional
from django import forms
from django.core.exceptions import ValidationError
from django.template.defaultfilters import filesizeformat
from django.utils.deconstruct import deconstructible

from LabCourse.api.models import SemesterDate, AppointmentRequest, Semester, LabUser
from LabCourse.mylab_tutor.utils import FileValidator


class DateInput(forms.DateInput):
    input_type = "date"


class RemoveAppointmentRequestForm(forms.Form):
    checked = forms.BooleanField(label="I really want to remove this request", required=True)


class ProcessAppointmentRequestForm(forms.Form):
    date = forms.ChoiceField(label="Date", required=True)
    newdate = forms.DateField(label="New date", required=False, widget=DateInput)
    students = forms.MultipleChoiceField(label="Students", required=False)
    send_email = forms.BooleanField(label="Send email confirmation", initial=True, required=False)

    def __init__(
        self, *args, requests: List[AppointmentRequest], dates: List[SemesterDate], date: SemesterDate, **kwargs
    ):
        forms.Form.__init__(self, *args, **kwargs)
        self.fields["students"].choices = ((r.student_id, r.student_id) for r in requests)
        self.fields["date"].choices = ((d.pk, d) for d in dates)
        self.fields["date"].initial = date.pk

        # get semester
        self.semester = None if len(dates) == 0 else dates[0].semester

    def clean(self):
        # clean
        super().clean()

        # get date
        newdate = self.cleaned_data.get("newdate")
        if newdate and self.semester:
            # inside semester?
            if newdate < self.semester.start or newdate > self.semester.end:
                raise ValidationError(
                    f"Given date is not within current semester ({self.semester.start} - {self.semester.end})."
                )

            # create or get it
            date, _ = SemesterDate.objects.get_or_create(
                semester=self.semester, date=newdate, defaults={"special": True}
            )

            # set it
            self.cleaned_data["date"] = date.pk


class AddAppointmentForm(forms.Form):
    experiment = forms.ChoiceField(label="Experiment", required=True)
    semester = forms.ChoiceField(label="Semester", required=True)
    date = forms.IntegerField(label="Date", required=True)
    newdate = forms.DateField(label="New date", required=False, widget=DateInput)
    student1 = forms.IntegerField(label="Student 1", required=False)
    student2 = forms.IntegerField(label="Student 2", required=False)
    student3 = forms.IntegerField(label="Student 3", required=False)
    send_email = forms.BooleanField(label="Send email confirmation", initial=True, required=False)

    def __init__(self, *args, experiments, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["experiment"].choices = ((e.code.lower(), f"{e.code} - {e.name}") for e in experiments)
        semesters = Semester.objects.all()
        self.fields["semester"].choices = (
            (s.code, f"{s} ({s.start.strftime('%Y-%m-%d')}-{s.end.strftime('%Y-%m-%d')})") for s in semesters
        )

    def clean(self):
        # clean
        super().clean()

        # get semester
        semester = Semester.objects.get(code=self.cleaned_data["semester"].replace("-", "/"))

        # get date
        newdate = self.cleaned_data.get("newdate")
        if newdate:
            # inside semester?
            if newdate < semester.start or newdate > semester.end:
                raise ValidationError(f"Given date is not within current semester ({semester.start} - {semester.end}).")

            # create or get it
            date, _ = SemesterDate.objects.get_or_create(semester=semester, date=newdate, defaults={"special": True})

            # set it
            self.cleaned_data["date"] = date.pk


class RemoveAppointmentForm(forms.Form):
    checked = forms.BooleanField(label="I really want to remove this appointment", required=True)
    send_email = forms.BooleanField(label="Send email confirmation", initial=True, required=False)


class CloseAppointmentForm(forms.Form):
    students = forms.MultipleChoiceField(label="Students", widget=forms.CheckboxSelectMultiple)

    def __init__(self, *args, appointment, **kwargs):
        super().__init__(*args, **kwargs)

        # add students as choices and select them
        self.fields["students"].choices = [(s.pk, s.full_name) for s in appointment.students.all()]
        self.fields["students"].initial = [i for i in appointment.students.all().values_list("id", flat=True)]


class AcceptReportForm(forms.Form):
    tutor = forms.ChoiceField(label="Tutor", required=True)
    passed = forms.ChoiceField(
        label="Result", required=True, choices=[(None, "---"), (True, "Passed"), (False, "Failed")]
    )
    send_email = forms.BooleanField(label="Send email confirmation", initial=True, required=False)

    def __init__(self, *args, appointment, user, **kwargs):
        super().__init__(*args, **kwargs)

        # add all tutors as choices
        tutor_choices = list(appointment.experiment.tutors.all())

        # if current user is admin...
        if user.is_admin:
            # add admin
            tutor_choices += list(LabUser.objects.filter(is_admin=True).all())

            # add all previous tutors
            tutor_choices += list(
                LabUser.objects.filter(tutor_experiments__in=[appointment.experiment]).distinct().all()
            )

        # set unique choices
        tutor_choices = list(set(tutor_choices))
        self.fields["tutor"].choices = [(s.pk, s.full_name) for s in tutor_choices]

        # select current user, if he is one of the choices (should be!)
        if user in tutor_choices:
            self.fields["tutor"].initial = user.pk


class UploadInstructionsForm(forms.Form):
    validate_file = FileValidator(max_size=1024 * 1024 * 10, content_types=("application/pdf",))
    experiment = forms.ChoiceField(label="Experiment")
    instructions = forms.FileField(label="Instructions PDF", required=True, validators=[validate_file])

    def __init__(self, *args, experiments, **kwargs):
        super().__init__(*args, **kwargs)

        # add experiments
        self.fields["experiment"].choices = [(e.pk, f"{e.code} - {e.name}") for e in experiments]
