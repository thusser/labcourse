from django.apps import AppConfig


class MylabAdminConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "LabCourse.mylab_tutor"
