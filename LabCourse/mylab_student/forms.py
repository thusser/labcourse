from typing import List
from django import forms
from django.core.exceptions import ValidationError

from LabCourse.api.models import Report, LabUser, SemesterDate, AppointmentRequest, Experiment
from LabCourse.mylab_tutor.utils import FileValidator


class UploadReportForm(forms.Form):
    validate_file = FileValidator(max_size=1024 * 1024 * 10, content_types=("application/pdf",))
    report = forms.FileField(label="Report PDF", required=True, validators=[validate_file])


class RequestJoinForm(forms.Form):
    pass


class AddExamForm(forms.Form):
    reports = forms.MultipleChoiceField(label="Labs", required=True)
    date = forms.DateTimeField(
        label="Date",
        widget=forms.DateTimeInput(format="%Y-%m-%dT%H:%M:%S", attrs={"type": "datetime-local"}),
        required=True,
    )
    course = forms.ChoiceField(label="Course", required=True, choices=[(1, "M.Phy.1401"), (2, "M.Phy.1402")], initial=1)
    examiner = forms.ChoiceField(label="Examiner", required=True)
    send_email = forms.BooleanField(label="Send email confirmation", initial=True, required=False)

    def __init__(self, *args, reports: List[Report], examiners: List[LabUser], **kwargs):
        forms.Form.__init__(self, *args, **kwargs)
        self.fields["reports"].choices = ((r.pk, r.pk) for r in reports)
        self.fields["examiner"].choices = ((e.pk, e.full_name) for e in examiners)


class RequestAppointmentForm(forms.Form):
    date = forms.ChoiceField(label="Date", required=True)
    partner1 = forms.ChoiceField(label="Partner 1", required=False)
    partner2 = forms.ChoiceField(label="Partner 2", required=False)
    comment = forms.CharField(max_length=50, label="Comment", required=False, widget=forms.Textarea(attrs={"rows": 3}))

    def __init__(
        self,
        *args,
        student: LabUser,
        experiment: Experiment,
        dates: List[SemesterDate],
        students: List[LabUser],
        **kwargs,
    ):
        self.student = student
        self.experiment = experiment
        forms.Form.__init__(self, *args, **kwargs)
        self.fields["date"].choices = ((d.pk, d.date) for d in dates)
        partners = tuple([("", "None")] + [(str(s.pk), s.full_name) for s in students])
        self.fields["partner1"].choices = partners
        self.fields["partner2"].choices = partners

    def clean(self):
        # clean
        super().clean()

        # does date exist?
        try:
            date = SemesterDate.objects.get(pk=self.cleaned_data["date"])
        except SemesterDate.DoesNotExist:
            raise ValidationError("Invalid date.")

        # do all users exist?
        if "partner1" in self.cleaned_data and self.cleaned_data["partner1"] != "":
            try:
                LabUser.objects.get(pk=self.cleaned_data["partner1"])
            except LabUser.DoesNotExist:
                raise ValidationError("Invalid partner.")
        if "partner2" in self.cleaned_data and self.cleaned_data["partner2"] != "":
            try:
                LabUser.objects.get(pk=self.cleaned_data["partner2"])
            except LabUser.DoesNotExist:
                raise ValidationError("Invalid partner.")

        # does a request already exist?
        if AppointmentRequest.objects.filter(date=date, student=self.student, experiment=self.experiment).count() > 0:
            raise ValidationError("An appointment request already exists for this date and experiment.")
