"""LabCourse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from .views import appointment, appointment_exp, student_help, request_join, upload_report, add_exam

urlpatterns = [
    path("appointment/", appointment, name="mylab.student.appointment"),
    path("appointment/<str:exp>/", appointment_exp, name="mylab.student.appointment.experiment"),
    path("help/", student_help, name="mylab.student.help"),
    path("join/", request_join, name="mylab.student.request_join"),
    path("report/<int:report_id>/upload", upload_report, name="mylab.student.upload_report"),
    path("exam/<str:username>", add_exam, name="mylab.student.add_exam"),
]
