import datetime
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.db.transaction import atomic
from django.http import Http404
from django.shortcuts import render

from LabCourse import settings
from LabCourse.mylab.email import (
    send_email_report_uploaded,
    send_email_join,
    send_email_new_exam,
    send_email_new_appointment_request,
)
from LabCourse.mylab_student.forms import UploadReportForm, RequestJoinForm, AddExamForm, RequestAppointmentForm
from LabCourse.settings import LABCOURSE
from LabCourse.api.models import (
    Semester,
    Category,
    Experiment,
    LabUser,
    SemesterDate,
    AppointmentRequest,
    JoinRequest,
    Report,
    Exam,
    ReportUpdate,
    UpdateModel,
    ExamUpdate,
)


@user_passes_test(lambda u: u.is_authenticated and (u.is_admin or u.is_student_now))
def appointment(request):
    # enabled?
    if not LABCOURSE["STUDENT"]["CAN_REQUEST_APPOINTMENT"]:
        raise Http404()

    # get current semester and dates
    semester = Semester.current()
    categories = Category.objects.all()

    # render
    return render(
        request,
        "mylab_student/appointment.html",
        {"mylab_page": "Student", "semester": semester, "categories": categories},
    )


@user_passes_test(lambda u: u.is_authenticated and (u.is_admin or u.is_student_now))
def appointment_exp(request, exp: str):
    # enabled?
    if not LABCOURSE["STUDENT"]["CAN_REQUEST_APPOINTMENT"]:
        raise Http404()

    # get semester, experiment, dates, and students
    semester = Semester.current()
    try:
        experiment = Experiment.objects.get(code=exp.upper(), active=True)
    except Experiment.DoesNotExist:
        raise Http404()
    dates = semester.semesterdate_set.filter(date__gt=datetime.date.today(), special=False)
    students = semester.students.exclude(pk=request.user.pk)

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = RequestAppointmentForm(
            request.POST, dates=dates, students=students, student=request.user, experiment=experiment
        )

        # check whether it's valid:
        if form.is_valid():
            # create request
            req = AppointmentRequest()
            req.student = request.user
            req.experiment = experiment
            req.date = SemesterDate.objects.get(pk=form.cleaned_data["date"])
            req.comment = form.cleaned_data["comment"]
            req.save()

            # add partners
            if "partner1" in form.cleaned_data and form.cleaned_data["partner1"] != "":
                req.partners.add(LabUser.objects.get(pk=form.cleaned_data["partner1"]))
            if "partner2" in form.cleaned_data and form.cleaned_data["partner2"] != "":
                req.partners.add(LabUser.objects.get(pk=form.cleaned_data["partner2"]))

            # send email to tutors and admins
            send_email_new_appointment_request(req)

            # render
            return render(
                request,
                "mylab_student/appointment_exp.html",
                {"success": True, "app_request": req},
            )

    else:
        # create form
        form = RequestAppointmentForm(dates=dates, students=students, student=request.user, experiment=experiment)

    # render
    return render(
        request,
        "mylab_student/appointment_exp.html",
        {
            "mylab_page": "Student",
            "form": form,
            "semester": semester,
            "experiment": experiment,
            "dates": dates,
            "students": students,
        },
    )


@user_passes_test(lambda u: u.is_authenticated and (u.is_student or u.is_admin))
def student_help(request):
    return render(request, "mylab_student/help.html")


@user_passes_test(lambda u: u.is_authenticated and (u.is_student or u.is_admin))
@atomic
def upload_report(request, report_id):
    # get report
    try:
        report = Report.objects.get(pk=report_id)
    except Report.DoesNotExist:
        raise Http404

    # check whether student owns report
    if not request.user.is_admin and report.student != request.user:
        raise PermissionDenied()

    # upload possible?
    if not report.accepts_upload(request.user):
        raise PermissionDenied()

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = UploadReportForm(request.POST, request.FILES)

        # check whether it's valid:
        if form.is_valid():
            # save report
            version = report.add_pdf(request.FILES["report"])

            # store update
            ReportUpdate(report=report, user=request.user, version=version, action=UpdateModel.Action.CHANGED).save()

            # send email to student and admins
            send_email_report_uploaded(report)

            # render
            return render(
                request,
                "mylab_student/upload_report.html",
                {"mylab_page": "Student", "report": report, "success": True},
            )

    else:
        # create form
        form = UploadReportForm()

    # render
    return render(
        request,
        "mylab_student/upload_report.html",
        {"mylab_page": "Student", "report": report, "form": form},
    )


@user_passes_test(lambda u: u.is_authenticated)
@atomic
def request_join(request):
    # enabled?
    if not LABCOURSE["STUDENT"]["CAN_JOIN"]:
        raise Http404()

    # get current semester
    semester = Semester.current()

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = RequestJoinForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            # add request
            join_request = JoinRequest(user=request.user)
            join_request.save()

            # send email
            send_email_join(join_request)

            # finished
            return render(
                request,
                "mylab_student/request_join.html",
                {"mylab_page": "Join", "semester": semester, "success": True},
            )

    else:
        # create form
        form = RequestJoinForm()

    # render
    return render(
        request,
        "mylab_student/request_join.html",
        {"mylab_page": "Join", "semester": semester, "form": form},
    )


@user_passes_test(lambda u: u.is_authenticated)
@atomic
def add_exam(request, username: str):
    # get user
    try:
        labuser = LabUser.objects.get(username=username)
    except LabUser.DoesNotExist:
        raise Http404

    # requesting user must either be admin or the user themself
    if labuser != request.user and not request.user.is_admin:
        raise Http404()

    # get reports for user
    reports = Report.finished(labuser)

    # get list of examiners
    examiners = LabUser.objects.filter(is_examiner=True).order_by("last_name", "first_name").all()

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddExamForm(request.POST, reports=reports, examiners=examiners)

        # cannot upload not accepted reports
        num_reports = settings.LABCOURSE["REPORTS_PER_EXAM"]
        if "reports" not in form.data:
            form.add_error(None, f"You need to select reports to add to the exam.")
        else:
            if len(form.data.getlist("reports")) != num_reports and not request.user.is_admin:
                form.add_error(None, f"You need to select {num_reports} reports to create an exam.")

        # check whether it's valid:
        if form.is_valid():
            # start a transaction
            with transaction.atomic():
                # create an exam
                exam = Exam()

                # add student, date and examiner
                exam.student = labuser
                exam.date = form.cleaned_data["date"]
                exam.examiner_id = form.cleaned_data["examiner"]
                exam.course = form.cleaned_data["course"]
                exam.save()

                # store update
                ExamUpdate(exam=exam, user=request.user, action=UpdateModel.Action.ADDED).save()

                # loop all reports and add them
                for report_id in form.cleaned_data["reports"]:
                    # get report
                    report = Report.objects.get(pk=report_id)
                    if report.date is None:
                        raise Http404()
                    exam.reports.add(report)

            # send email to student, examiner and admins
            if "send_email" not in form.cleaned_data or form.cleaned_data["send_email"] is True:
                send_email_new_exam(exam)

            # finished
            return render(
                request,
                "mylab_student/add_exam.html",
                {"mylab_page": "Student", "success": True},
            )

    else:
        # create form
        form = AddExamForm(reports=reports, examiners=examiners)

    # render
    return render(
        request,
        "mylab_student/add_exam.html",
        {"mylab_page": "Student", "reports": reports, "form": form, "labuser": labuser},
    )
